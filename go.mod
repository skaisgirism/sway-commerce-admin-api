module bitbucket.org/sway-commerce/sway-commerce-admin-api

go 1.13

require (
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/go-chi/cors v1.0.0
	github.com/go-sql-driver/mysql v1.4.1
	github.com/jmoiron/sqlx v1.2.0
	github.com/labstack/gommon v0.3.0
	github.com/shopspring/decimal v0.0.0-20190905144223-a36b5d85f337
	github.com/sirupsen/logrus v1.4.2
	google.golang.org/appengine v1.6.4 // indirect
	gopkg.in/yaml.v2 v2.2.4
)
