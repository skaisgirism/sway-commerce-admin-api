package user

import (
	"github.com/go-chi/chi"
	"net/http"
)

func UserRouter() http.Handler {
	r := chi.NewRouter()
	r.Post("/create", createUser)
	//r.Get("/{EntityID}", getUser)
	//r.Get("/", getUserList)
	//r.Delete("/{EntityID}", deleteUser)
	//r.Put("/{EntityID}", updateUser)
	return r
}
