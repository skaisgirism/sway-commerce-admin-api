package config

import (
	"bitbucket.org/sway-commerce/sway-commerce-admin-api/helpers"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	sql "github.com/jmoiron/sqlx"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"os"
)

func GetConfig(configFile string) *Config {
	Conf = &Config{}
	if configFile != "" {
		err := Conf.GetConfFromFile(configFile)
		helpers.PanicErr(err)
	}
	return Conf
}

func (c *Config) GetDb() (*sql.DB, error) {
	if db == nil {
		if dbUri == "" {
			dbConfig := c.Db
			driverName = dbConfig.DriverName
			dbUri = fmt.Sprintf("%s:%s@%s/%s?charset=%s&parseTime=True",
				dbConfig.User, dbConfig.Password,
				dbConfig.Server, dbConfig.DbName, dbConfig.Charset)
		}
		ldb, err := sql.Open(driverName, dbUri)
		if err != nil {
			return nil, err
		}
		db = ldb
	}
	return db, nil
}

func (c *Config) GetConfFromFile(fileName string) error {
	pwd, _ := os.Getwd()
	yamlFile, err := ioutil.ReadFile(pwd + "/" + fileName)
	if err != nil {
		log.Printf("%s file read error.  #%v\n", fileName, err)
	}
	return c.GetConfFromString(string(yamlFile))
}

func (c *Config) GetConfFromString(yamlString string) error {

	err := yaml.Unmarshal([]byte(yamlString), c)
	if err != nil {
		log.Fatalf("%s parse error %v\n", yamlString, err)
	}
	return err
}
