package solrProduct

import (
	"bitbucket.org/sway-commerce/sway-commerce-admin-api/helpers"
	"bitbucket.org/sway-commerce/sway-commerce-admin-api/solr"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
)

//Decodes body from URL and returns a SalonPro product array
func LoadProductDataFromUrl(url string) ([]solr.SPProduct, error) {
	//url := "https://www.salonpro.lt/productsjson?page=1"
	resp, err := http.Get(url)
	helpers.CheckErr(err)
	defer resp.Body.Close()
	productList := make([]solr.SPProduct, 0)
	err = json.NewDecoder(resp.Body).Decode(&productList)
	if err != nil && err.Error() != "EOF" {
		return nil, err
	}
	return productList, nil
}

//Method loads data from json file
func LoadInfo(fileName string) ([]solr.Product, error) {
	var data []solr.Product
	infoFile, err := os.Open(fileName)
	if err != nil {
		//return data, err
		log.Fatal(err)
	}
	defer infoFile.Close()
	fmt.Println(fileName, " Opened")

	byteValue, _ := ioutil.ReadAll(infoFile)
	json.Unmarshal(byteValue, &data)

	//jsonFile, err := os.Create("./data.json")
	//jsonFile.Write(byteValue)

	var solrData solr.SolrDataStruct
	solrData = ChangeStructs(data)
	//fmt.Println(len(solrData.ConfigurableProductStruct))
	solrData = modifyDataAmount(solrData, 5000)
	SplitCatalog(solrData, 1000)

	return data, err
}

//maps products to vue struct
func VueProductListMapper(productList []solr.SPProduct) ([]solr.Product, error) {
	var vueProductList []solr.Product
	var err error

	var otherAttributes solr.OtherAttributes

	for i := range productList {
		var vueProduct = NewProduct()
		salonProduct := productList[i]
		err = json.Unmarshal(salonProduct.OtherAttributes, &otherAttributes)
		helpers.CheckErr(err)

		//otherAttributes.Image = "/w/b/wb03-purple-0.jpg"
		//otherAttributes.SmallImage = "/w/b/wb03-purple-0.jpg"
		//otherAttributes.Thumbnail = "/w/b/wb03-purple-0.jpg"

		if salonProduct.Manufacturer == false {
			salonProduct.Manufacturer = nil
		}

		if salonProduct.Brand == false {
			salonProduct.Brand = nil
		}

		if salonProduct.IsInStock == 0 {
			salonProduct.IsInStock = false
		}
		if salonProduct.IsInStock == 1 {
			salonProduct.IsInStock = true
		}

		if salonProduct.Supplier == false {
			salonProduct.Supplier = nil
		}

		if salonProduct.TypeID == "virtual" {
			fmt.Println("product sku: " + salonProduct.Sku + " was virtual")
		}

		if salonProduct.TypeID == "configurable" {
			fmt.Println("product sku: " + salonProduct.Sku + " was virtual")
		} else {
			vueProduct.ID = salonProduct.EntityID
			//testing change
			//vueProduct.DocType = "parentDoc"
			vueProduct.Source.Sku = salonProduct.Sku
			vueProduct.Source.ID = extractInt(salonProduct.EntityID)
			vueProduct.Source.Name = salonProduct.Name
			vueProduct.Source.AttributeSetID = extractInt(otherAttributes.AttributeSetID)
			vueProduct.Source.Price = extractFloat(salonProduct.Price)
			vueProduct.Source.Status = extractInt(salonProduct.Status)
			vueProduct.Source.Visibility = extractInt(salonProduct.Visibility)
			vueProduct.Source.TypeID = salonProduct.TypeID
			vueProduct.Source.CreatedAt = salonProduct.CreatedAt
			vueProduct.Source.UpdatedAt = salonProduct.UpdatedAt
			vueProduct.Source.Weight = 1
			vueProduct.Source.FinalPrice = extractFloat(salonProduct.Price)
			vueProduct.Source.MaxPrice = extractFloat(salonProduct.Price)
			vueProduct.Source.MaxRegularPrice = extractFloat(salonProduct.Price)
			vueProduct.Source.MinimalRegularPrice = extractFloat(salonProduct.Price)
			vueProduct.Source.MinimalPrice = extractFloat(salonProduct.Price)
			vueProduct.Source.RegularPrice = extractFloat(salonProduct.Price)
			vueProduct.Source.Tsk = 1530103332268
			vueProduct.Source.Stock.ItemID = extractInt(salonProduct.EntityID)
			vueProduct.Source.Stock.ProductID = extractInt(salonProduct.EntityID)
			//testing change
			vueProduct.Source.Stock.StockID = 1
			vueProduct.Source.Stock.Qty = salonProduct.Qty
			vueProduct.Source.Stock.IsInStock = extractBool(salonProduct.IsInStock)
			vueProduct.Source.Description = salonProduct.Description
			vueProduct.Source.Image = otherAttributes.Image
			vueProduct.Source.SmallImage = otherAttributes.SmallImage
			vueProduct.Source.Thumbnail = otherAttributes.Thumbnail
			vueProduct.Source.OptionsContainer = otherAttributes.OptionsContainer
			vueProduct.Source.RequiredOptions = otherAttributes.RequiredOptions
			vueProduct.Source.HasOptions = otherAttributes.HasOptions
			vueProduct.Source.URLKey = otherAttributes.URLKey
			vueProduct.Source.TaxClassID = otherAttributes.TaxClassID

			// media_gallery
			var mediaGallery solr.MediaGallery
			mediaGallery.TypeOf = "media_gallery"
			mediaGallery.Image = otherAttributes.Image
			mediaGallery.Pos = 1
			mediaGallery.Typ = "image"
			mediaGallery.Lab = ""
			vueProduct.Source.MediaGallery = append(vueProduct.Source.MediaGallery, mediaGallery)

			//categories
			var category solr.Category
			//category.CategoryID = extractInt(salonProduct.Categories.)
			for i := range salonProduct.Categories{
				category.Name = salonProduct.Categories[i].Name
				category.CategoryID = extractInt(salonProduct.Categories[i].ID)
				vueProduct.Source.Category = append(vueProduct.Source.Category, category)
			}
			vueProductList = append(vueProductList, vueProduct)
		}
	}
	dataJson, err := json.Marshal(vueProductList)
	jsonFile, err := os.Create("./data.json")
	jsonFile.Write(dataJson)
	helpers.CheckErr(err)
	return vueProductList, err
}

// Function that takes SolrDataStruct parameter,
// increases amount of data stored in []SimpleProductStruct
// and []ConfigurableProductStruct to int got from amount parameter
func modifyDataAmount(dataStruct solr.SolrDataStruct, amount int) solr.SolrDataStruct {
	var simpleProductArray []solr.SimpleProductStruct
	var configurableProductArray []solr.ConfigurableProductStruct
	var initialSimpleProductArrayLength = len(dataStruct.SimpleProductStruct) - 1
	var initialConfigurableArrayLength = len(dataStruct.ConfigurableProductStruct) - 1

	//More simple products
	for i := amount; i > 0; i-- {
		if initialSimpleProductArrayLength >= 0 {
			dataStruct.SimpleProductStruct[initialSimpleProductArrayLength].ID = "simple_product_" + strconv.Itoa(i)
			dataStruct.SimpleProductStruct[initialSimpleProductArrayLength].Source.ID = i
			simpleProductArray = append(simpleProductArray, dataStruct.SimpleProductStruct[initialSimpleProductArrayLength])
			if initialSimpleProductArrayLength > 0 {
				initialSimpleProductArrayLength = initialSimpleProductArrayLength - 1
			} else {
				initialSimpleProductArrayLength = len(dataStruct.SimpleProductStruct) - 1
			}
		}
	}

	//More configurable products
	for i := amount; i > 0; i-- {
		if initialConfigurableArrayLength >= 0 {
			//----
			myCopy := dataStruct.ConfigurableProductStruct[initialConfigurableArrayLength]
			myCopy.Source.ChildDocuments = make([]interface{}, len(dataStruct.ConfigurableProductStruct[initialConfigurableArrayLength].Source.ChildDocuments))
			copy(myCopy.Source.ChildDocuments, dataStruct.ConfigurableProductStruct[initialConfigurableArrayLength].Source.ChildDocuments)
			myCopy.ID = "configurable_product_" + strconv.Itoa(amount+i+1)
			myCopy.Source.ID = amount + i + 1
			configurableProductArray = append(configurableProductArray, myCopy)

			//---
			//dataStruct.ConfigurableProductStruct[initialConfigurableArrayLength].ID = "configurable_product_" + strconv.Itoa(amount+i+1)
			//dataStruct.ConfigurableProductStruct[initialConfigurableArrayLength].Source.ID = amount + i + 1
			//configurableProductArray = append(configurableProductArray, dataStruct.ConfigurableProductStruct[initialConfigurableArrayLength])
			if initialConfigurableArrayLength > 0 {
				initialConfigurableArrayLength = initialConfigurableArrayLength - 1
			} else {
				initialConfigurableArrayLength = len(dataStruct.ConfigurableProductStruct) - 1
			}
		}
	}
	// fmt.Println(configurableProductArray[5].Source.ChildDocuments[4].(ConfigurableChildren).ID)

	z := amount*3 + 1
	for i := range configurableProductArray {
		for j := range configurableProductArray[i].Source.ChildDocuments {
			v, ok := configurableProductArray[i].Source.ChildDocuments[j].(solr.ConfigurableChildren)
			if ok {
				v.ID = z
				z = z + 1
				configurableProductArray[i].Source.ChildDocuments[j] = v
				//fmt.Println(d.ID, " - ParentID ", configurableProductArray[i].Source.ID)
			}

			h, ok := configurableProductArray[i].Source.ChildDocuments[j].(solr.ConfigurableOptions)
			if ok {
				h.ProductID = configurableProductArray[i].Source.ID
				configurableProductArray[i].Source.ChildDocuments[j] = h
			}
		}
	}

	//Modified data assigned to rest of data
	dataStruct.SimpleProductStruct = simpleProductArray
	dataStruct.ConfigurableProductStruct = configurableProductArray

	fmt.Println("Simple product length: ", len(dataStruct.SimpleProductStruct))
	fmt.Println("Configurable product length: ", len(dataStruct.ConfigurableProductStruct))

	return dataStruct

}

// Function that takes SolrDataStrut as parameter,
// splits it to pieces defined by size parameter,
// and calls PushCategories() functions for each piece
func SplitCatalog(catalog solr.SolrDataStruct, size int) {
	//Simple product
	if len(catalog.SimpleProductStruct) > size {
		var slicesAmount = 1 + len(catalog.SimpleProductStruct)/size
		var startFrom = 0
		catalogSlices := make([][]solr.SimpleProductStruct, slicesAmount)
		for i := 1; i < slicesAmount+1; i++ {
			if size*i > len(catalog.SimpleProductStruct) {
				var length = len(catalog.SimpleProductStruct)
				catalogSlices[i-1] = append(catalog.SimpleProductStruct[startFrom:length])
			} else {
				catalogSlices[i-1] = append(catalog.SimpleProductStruct[startFrom : size*i])
				startFrom = startFrom + size
			}
			PushCategories(catalogSlices[i-1])
		}
	} else {
		PushCategories(catalog.SimpleProductStruct)
	}

	//Configurable product
	if len(catalog.ConfigurableProductStruct) > size {
		var slicesAmount = 1 + len(catalog.ConfigurableProductStruct)/size
		var startFrom = 0
		catalogSlices := make([][]solr.ConfigurableProductStruct, slicesAmount)
		for i := 1; i < slicesAmount+1; i++ {
			if size*i > len(catalog.ConfigurableProductStruct) {
				var length = len(catalog.ConfigurableProductStruct)
				catalogSlices[i-1] = append(catalog.ConfigurableProductStruct[startFrom:length])
			} else {
				catalogSlices[i-1] = append(catalog.ConfigurableProductStruct[startFrom : size*i])
				startFrom = startFrom + size
			}
			//for g := range catalogSlices {
			//	for h := range catalogSlices[g] {
			//		fmt.Println(catalogSlices[g][h].Source.ChildDocuments)
			//	}
			//}
			PushCategories(catalogSlices[i-1])
		}
	} else {
		PushCategories(catalog.ConfigurableProductStruct)
	}

	//Category
	if len(catalog.CategoryStruct) > size {
		var slicesAmount = 1 + len(catalog.CategoryStruct)/size
		var startFrom = 0
		catalogSlices := make([][]solr.CategoryStruct, slicesAmount)
		for i := 1; i < slicesAmount+1; i++ {
			if size*i > len(catalog.CategoryStruct) {
				var length = len(catalog.CategoryStruct)
				catalogSlices[i-1] = append(catalog.CategoryStruct[startFrom:length])
			} else {
				catalogSlices[i-1] = append(catalog.CategoryStruct[startFrom : size*i])
				startFrom = startFrom + size
			}
			PushCategories(catalogSlices[i-1])
		}
	} else {
		PushCategories(catalog.CategoryStruct)
	}

	//Attribute
	if len(catalog.AttributeStruct) > size {
		var slicesAmount = 1 + len(catalog.AttributeStruct)/size
		var startFrom = 0
		catalogSlices := make([][]solr.AttributeStruct, slicesAmount)
		for i := 1; i < slicesAmount+1; i++ {
			if size*i > len(catalog.AttributeStruct) {
				var length = len(catalog.AttributeStruct)
				catalogSlices[i-1] = append(catalog.AttributeStruct[startFrom:length])
			} else {
				catalogSlices[i-1] = append(catalog.AttributeStruct[startFrom : size*i])
				startFrom = startFrom + size
			}
			PushCategories(catalogSlices[i-1])
		}
	} else {
		PushCategories(catalog.AttributeStruct)
	}

	//TaxRule
	if len(catalog.TaxRuleStruct) > size {
		var slicesAmount = 1 + len(catalog.TaxRuleStruct)/size
		var startFrom = 0
		catalogSlices := make([][]solr.TaxRuleStruct, slicesAmount)
		for i := 1; i < slicesAmount+1; i++ {
			if size*i > len(catalog.TaxRuleStruct) {
				var length = len(catalog.TaxRuleStruct)
				catalogSlices[i-1] = append(catalog.TaxRuleStruct[startFrom:length])
			} else {
				catalogSlices[i-1] = append(catalog.TaxRuleStruct[startFrom : size*i])
				startFrom = startFrom + size
			}
			PushCategories(catalogSlices[i-1])
		}
	} else {
		PushCategories(catalog.TaxRuleStruct)
	}
}

// Function that takes interface as parameter,
// pushes it to Solr search engine
// with different parameters depending on data type
// and logs out response to console
func PushCategories(data interface{}) {
	//Makes copy of struct to Buffer
	structCopy := new(bytes.Buffer)
	//Encodes it
	json.NewEncoder(structCopy).Encode(data)
	//Sends encoded structCopy to Solr search engine
	//Post method url contains all the parameters how Solar should handle the body
	resp, err := http.Post("http://167.71.10.66:8983/solr/salonPro/update/json/docs?"+
		//resp, err := http.Post("http://192.168.0.2:8983/solr/storefrontCore/update/json/docs?"+
		"split=/_source"+
		"|/_source/_childDocuments_"+
		"&f=/**"+
		"&commit=true",
		helpers.MIMEApplicationJSON, structCopy)
	//Catch error
	if err != nil {
		fmt.Println(err)
	}
	//log our response
	b, _ := ioutil.ReadAll(resp.Body)
	fmt.Printf("%s", b)
}
