package solrProduct

import (
	"bitbucket.org/sway-commerce/sway-commerce-admin-api/helpers"
	"encoding/json"
	"net/http"
)

func productDataPushToSolr(w http.ResponseWriter, r *http.Request) {
	salonProductList, err := LoadProductDataFromUrl("https://www.salonpro.lt/productsjson?page=1")
	helpers.CheckErr(err)

	data, err := VueProductListMapper(salonProductList)
	helpers.CheckErr(err)

	//var solrData SolrDataStructc
	solrData := ChangeStructs(data)

	solrData = modifyDataAmount(solrData, 5000)
	SplitCatalog(solrData, 1000)

	if err == nil {
		json.NewEncoder(w).Encode("ok")
	} else {
		json.NewEncoder(w).Encode(err)
	}
}
