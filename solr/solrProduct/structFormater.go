package solrProduct

import (
	"bitbucket.org/sway-commerce/sway-commerce-admin-api/solr"
	//category "sway-commerce-admin-api/solr/category"
	//"sway-commerce-admin-api/solr/category"
)

func FromProductToSimpleProduct(product solr.Product) solr.SimpleProductStruct {
	var simpleProduct solr.SimpleProductStruct

	simpleProduct.Index = product.Index
	simpleProduct.Type = product.Type
	simpleProduct.ID = product.ID + "_" + product.Type
	simpleProduct.Score = product.Score
	simpleProduct.DocType = "parentDoc"

	//_source
	simpleProduct.Source.ID = product.Source.ID
	simpleProduct.Source.Sku = product.Source.Sku
	simpleProduct.Source.Name = product.Source.Name
	simpleProduct.Source.AttributeSetID = product.Source.AttributeSetID
	simpleProduct.Source.Price = product.Source.Price
	simpleProduct.Source.Status = product.Source.Status
	simpleProduct.Source.Visibility = product.Source.Visibility
	simpleProduct.Source.TypeID = product.Source.TypeID
	simpleProduct.Source.CreatedAt = product.Source.CreatedAt
	simpleProduct.Source.UpdatedAt = product.Source.UpdatedAt
	simpleProduct.Source.CustomAttributes = product.Source.CustomAttributes
	simpleProduct.Source.FinalPrice = product.Source.FinalPrice
	simpleProduct.Source.MaxPrice = product.Source.MaxPrice
	simpleProduct.Source.MaxRegularPrice = product.Source.MaxRegularPrice
	simpleProduct.Source.MinimalRegularPrice = product.Source.MinimalRegularPrice
	simpleProduct.Source.MinimalPrice = product.Source.MinimalPrice
	simpleProduct.Source.RegularPrice = product.Source.RegularPrice
	simpleProduct.Source.Tsk = product.Source.Tsk
	simpleProduct.Source.Description = product.Source.Description
	simpleProduct.Source.Image = product.Source.Image
	simpleProduct.Source.SmallImage = product.Source.SmallImage
	simpleProduct.Source.Thumbnail = product.Source.Thumbnail
	simpleProduct.Source.CategoryIds = product.Source.CategoryIds
	simpleProduct.Source.OptionsContainer = product.Source.OptionsContainer
	simpleProduct.Source.RequiredOptions = product.Source.RequiredOptions
	simpleProduct.Source.HasOptions = product.Source.HasOptions
	simpleProduct.Source.URLKey = product.Source.URLKey
	simpleProduct.Source.TaxClassID = product.Source.TaxClassID
	simpleProduct.Source.Activity = product.Source.Activity
	simpleProduct.Source.Material = product.Source.Material
	simpleProduct.Source.Gender = product.Source.Gender
	simpleProduct.Source.CategoryGear = product.Source.CategoryGear
	simpleProduct.Source.ErinRecommends = product.Source.ErinRecommends
	simpleProduct.Source.New = product.Source.New

	//stock
	simpleProduct.Source.Stock.ItemID = product.Source.Stock.ItemID
	simpleProduct.Source.Stock.ProductID = product.Source.Stock.ProductID
	simpleProduct.Source.Stock.StockID = product.Source.Stock.StockID
	simpleProduct.Source.Stock.Qty = product.Source.Stock.Qty
	simpleProduct.Source.Stock.IsInStock = product.Source.Stock.IsInStock
	simpleProduct.Source.Stock.IsQtyDecimal = product.Source.Stock.IsQtyDecimal
	simpleProduct.Source.Stock.ShowDefaultNotificationMessage = product.Source.Stock.ShowDefaultNotificationMessage
	simpleProduct.Source.Stock.UseConfigMinQty = product.Source.Stock.UseConfigMinQty
	simpleProduct.Source.Stock.MinQty = product.Source.Stock.MinQty
	simpleProduct.Source.Stock.UseConfigMinSaleQty = product.Source.Stock.UseConfigMinSaleQty
	simpleProduct.Source.Stock.MinSaleQty = product.Source.Stock.MinSaleQty
	simpleProduct.Source.Stock.UseConfigMaxSaleQty = product.Source.Stock.UseConfigMaxSaleQty
	simpleProduct.Source.Stock.MaxSaleQty = product.Source.Stock.MaxSaleQty
	simpleProduct.Source.Stock.UseConfigBackorders = product.Source.Stock.UseConfigBackorders
	simpleProduct.Source.Stock.Backorders = product.Source.Stock.Backorders
	simpleProduct.Source.Stock.UseConfigNotifyStockQty = product.Source.Stock.UseConfigNotifyStockQty
	simpleProduct.Source.Stock.NotifyStockQty = product.Source.Stock.NotifyStockQty
	simpleProduct.Source.Stock.UseConfigQtyIncrements = product.Source.Stock.UseConfigQtyIncrements
	simpleProduct.Source.Stock.QtyIncrements = product.Source.Stock.QtyIncrements
	simpleProduct.Source.Stock.UseConfigEnableQtyInc = product.Source.Stock.UseConfigEnableQtyInc
	simpleProduct.Source.Stock.EnableQtyIncrements = product.Source.Stock.EnableQtyIncrements
	simpleProduct.Source.Stock.UseConfigManageStock = product.Source.Stock.UseConfigManageStock
	simpleProduct.Source.Stock.ManageStock = product.Source.Stock.ManageStock
	simpleProduct.Source.Stock.LowStockDate = product.Source.Stock.LowStockDate
	simpleProduct.Source.Stock.IsDecimalDivided = product.Source.Stock.IsDecimalDivided
	simpleProduct.Source.Stock.StockStatusChangedAuto = product.Source.Stock.StockStatusChangedAuto

	// media_gallery
	for i := range product.Source.MediaGallery {

		product.Source.MediaGallery[i].TypeOf = "media_gallery"

		simpleProduct.Source.ChildDocuments = append(simpleProduct.Source.ChildDocuments, product.Source.MediaGallery[i])
	}

	// category
	for i := range product.Source.Category {

		product.Source.Category[i].TypeOf = "category"

		simpleProduct.Source.ChildDocuments = append(simpleProduct.Source.ChildDocuments, product.Source.Category[i])
	}

	// productLinks
	for i := range product.Source.ProductLinks {

		product.Source.ProductLinks[i].TypeOf = "product_links"

		simpleProduct.Source.ChildDocuments = append(simpleProduct.Source.ChildDocuments, product.Source.ProductLinks[i])
	}

	//Tier_Prices
	for i := range product.Source.TierPrices {
		product.Source.TierPrices[i].TypeOf = "tier_prices"

		simpleProduct.Source.ChildDocuments = append(simpleProduct.Source.ChildDocuments, product.Source.TierPrices[i])
	}

	return simpleProduct
}

func FromProductToConfigurableProduct(product solr.Product) solr.ConfigurableProductStruct {
	var configurableProduct solr.ConfigurableProductStruct

	configurableProduct.Index = product.Index
	configurableProduct.Type = product.Type
	configurableProduct.ID = product.ID + "_" + product.Type
	configurableProduct.Score = product.Score
	configurableProduct.DocType = "parentDoc"

	//_source
	configurableProduct.Source.ID = product.Source.ID
	configurableProduct.Source.Sku = product.Source.Sku
	configurableProduct.Source.Name = product.Source.Name
	configurableProduct.Source.AttributeSetID = product.Source.AttributeSetID
	configurableProduct.Source.Price = product.Source.Price
	configurableProduct.Source.Status = product.Source.Status
	configurableProduct.Source.Visibility = product.Source.Visibility
	configurableProduct.Source.TypeID = product.Source.TypeID
	configurableProduct.Source.CreatedAt = product.Source.CreatedAt
	configurableProduct.Source.UpdatedAt = product.Source.UpdatedAt
	configurableProduct.Source.CustomAttributes = product.Source.CustomAttributes
	configurableProduct.Source.FinalPrice = product.Source.FinalPrice
	configurableProduct.Source.MaxPrice = product.Source.MaxPrice
	configurableProduct.Source.MaxRegularPrice = product.Source.MaxRegularPrice
	configurableProduct.Source.MinimalRegularPrice = product.Source.MinimalRegularPrice
	configurableProduct.Source.MinimalPrice = product.Source.MinimalPrice
	configurableProduct.Source.RegularPrice = product.Source.RegularPrice
	configurableProduct.Source.ColorOptions = product.Source.ColorOptions
	configurableProduct.Source.SizeOptions = product.Source.SizeOptions
	configurableProduct.Source.Tsk = product.Source.Tsk
	configurableProduct.Source.Description = product.Source.Description
	configurableProduct.Source.Image = product.Source.Image
	configurableProduct.Source.SmallImage = product.Source.SmallImage
	configurableProduct.Source.Thumbnail = product.Source.Thumbnail
	configurableProduct.Source.CategoryIds = product.Source.CategoryIds
	configurableProduct.Source.OptionsContainer = product.Source.OptionsContainer
	configurableProduct.Source.RequiredOptions = product.Source.RequiredOptions
	configurableProduct.Source.HasOptions = product.Source.HasOptions
	configurableProduct.Source.URLKey = product.Source.URLKey
	configurableProduct.Source.MsrpDisplayActualPriceType = product.Source.MsrpDisplayActualPriceType
	configurableProduct.Source.TaxClassID = product.Source.TaxClassID
	configurableProduct.Source.Material = product.Source.Material
	configurableProduct.Source.EcoCollection = product.Source.EcoCollection
	configurableProduct.Source.PerformanceFabric = product.Source.PerformanceFabric
	configurableProduct.Source.ErinRecommends = product.Source.ErinRecommends
	configurableProduct.Source.New = product.Source.New
	configurableProduct.Source.Sale = product.Source.Sale
	configurableProduct.Source.Pattern = product.Source.Pattern
	configurableProduct.Source.Climate = product.Source.Climate

	//stock
	configurableProduct.Source.Stock.ItemID = product.Source.Stock.ItemID
	configurableProduct.Source.Stock.ProductID = product.Source.Stock.ProductID
	configurableProduct.Source.Stock.StockID = product.Source.Stock.StockID
	configurableProduct.Source.Stock.Qty = product.Source.Stock.Qty
	configurableProduct.Source.Stock.IsInStock = product.Source.Stock.IsInStock
	configurableProduct.Source.Stock.IsQtyDecimal = product.Source.Stock.IsQtyDecimal
	configurableProduct.Source.Stock.ShowDefaultNotificationMessage = product.Source.Stock.ShowDefaultNotificationMessage
	configurableProduct.Source.Stock.UseConfigMinQty = product.Source.Stock.UseConfigMinQty
	configurableProduct.Source.Stock.MinQty = product.Source.Stock.MinQty
	configurableProduct.Source.Stock.UseConfigMinSaleQty = product.Source.Stock.UseConfigMinSaleQty
	configurableProduct.Source.Stock.MinSaleQty = product.Source.Stock.MinSaleQty
	configurableProduct.Source.Stock.UseConfigMaxSaleQty = product.Source.Stock.UseConfigMaxSaleQty
	configurableProduct.Source.Stock.MaxSaleQty = product.Source.Stock.MaxSaleQty
	configurableProduct.Source.Stock.UseConfigBackorders = product.Source.Stock.UseConfigBackorders
	configurableProduct.Source.Stock.Backorders = product.Source.Stock.Backorders
	configurableProduct.Source.Stock.UseConfigNotifyStockQty = product.Source.Stock.UseConfigNotifyStockQty
	configurableProduct.Source.Stock.NotifyStockQty = product.Source.Stock.NotifyStockQty
	configurableProduct.Source.Stock.UseConfigQtyIncrements = product.Source.Stock.UseConfigQtyIncrements
	configurableProduct.Source.Stock.QtyIncrements = product.Source.Stock.QtyIncrements
	configurableProduct.Source.Stock.UseConfigEnableQtyInc = product.Source.Stock.UseConfigEnableQtyInc
	configurableProduct.Source.Stock.EnableQtyIncrements = product.Source.Stock.EnableQtyIncrements
	configurableProduct.Source.Stock.UseConfigManageStock = product.Source.Stock.UseConfigManageStock
	configurableProduct.Source.Stock.ManageStock = product.Source.Stock.ManageStock
	configurableProduct.Source.Stock.LowStockDate = product.Source.Stock.LowStockDate
	configurableProduct.Source.Stock.IsDecimalDivided = product.Source.Stock.IsDecimalDivided
	configurableProduct.Source.Stock.StockStatusChangedAuto = product.Source.Stock.StockStatusChangedAuto

	//media_gallery
	for i := range product.Source.MediaGallery {

		product.Source.MediaGallery[i].TypeOf = "media_gallery"

		configurableProduct.Source.ChildDocuments = append(configurableProduct.Source.ChildDocuments, product.Source.MediaGallery[i])
	}

	//configurable_children
	for i := range product.Source.ConfigurableChildren {
		product.Source.ConfigurableChildren[i].TypeOf = "configurable_children"

		configurableProduct.Source.ChildDocuments = append(configurableProduct.Source.ChildDocuments, product.Source.ConfigurableChildren[i])
	}
	//configurable_options
	for i := range product.Source.ConfigurableOptions {
		var tempOptions solr.ConfigurableOptions
		tempOptions.TypeOf = "configurable_options"

		tempOptions.ConfigurableOptionsId = product.Source.ConfigurableOptions[i].ID
		tempOptions.AttributeID = product.Source.ConfigurableOptions[i].AttributeID
		tempOptions.Label = product.Source.ConfigurableOptions[i].Label
		tempOptions.Position = product.Source.ConfigurableOptions[i].Position
		tempOptions.ProductID = product.Source.ConfigurableOptions[i].ProductID
		tempOptions.AttributeCode = product.Source.ConfigurableOptions[i].AttributeCode
		tempOptions.Values = product.Source.ConfigurableOptions[i].Values

		configurableProduct.Source.ChildDocuments = append(configurableProduct.Source.ChildDocuments, tempOptions)

	}

	//solrCategory
	for i := range product.Source.Category {
		product.Source.Category[i].TypeOf = "solrCategory"

		configurableProduct.Source.ChildDocuments = append(configurableProduct.Source.ChildDocuments, product.Source.Category[i])
	}

	// productLinks
	for i := range product.Source.ProductLinks {
		product.Source.ProductLinks[i].TypeOf = "product_links"

		configurableProduct.Source.ChildDocuments = append(configurableProduct.Source.ChildDocuments, product.Source.ProductLinks[i])
	}

	//Tier_Prices
	for i := range product.Source.TierPrices {
		product.Source.TierPrices[i].TypeOf = "tier_prices"

		configurableProduct.Source.ChildDocuments = append(configurableProduct.Source.ChildDocuments, product.Source.TierPrices[i])
	}
	return configurableProduct
}

func FromProductToCategory(product solr.Product) solr.CategoryStruct {
	var category solr.CategoryStruct

	category.Index = product.Index
	category.Type = product.Type
	category.ID = product.ID + "_" + product.Type
	category.Score = product.Score

	//_source
	category.Source.ID = product.Source.ID
	category.Source.ParentID = product.Source.ParentId
	category.Source.Name = product.Source.Name
	category.Source.IsActive = product.Source.IsActive
	category.Source.Position = product.Source.Position
	category.Source.Level = product.Source.Level
	category.Source.ProductCount = product.Source.ProductCount
	category.Source.Children = product.Source.Children
	category.Source.CreatedAt = product.Source.CreatedAt
	category.Source.UpdatedAt = product.Source.UpdatedAt
	category.Source.Path = product.Source.Path
	category.Source.AvailableSortBy = product.Source.AvailableSortBy
	category.Source.IncludeInMenu = product.Source.IncludeInMenu
	category.Source.IsAnchor = product.Source.IsAnchor
	category.Source.ChildrenCount = product.Source.ChildrenCount
	category.Source.URLKey = product.Source.URLKey
	category.Source.URLPath = product.Source.URLPath
	category.Source.Tsk = product.Source.Tsk

	//children_data
	//category.Source.ChildrenData = make([]ChildrenData, len(product.Source.ChildrenData))
	//for i := range product.Source.ChildrenData {
	//	category.Source.ChildrenData[i].ID = product.Source.ChildrenData[i].ID
	//	category.Source.ChildrenData[i].ParentID = product.Source.ChildrenData[i].ParentID
	//	category.Source.ChildrenData[i].Name = product.Source.ChildrenData[i].Name
	//	category.Source.ChildrenData[i].IsActive = product.Source.ChildrenData[i].IsActive
	//	category.Source.ChildrenData[i].Position = product.Source.ChildrenData[i].Position
	//	category.Source.ChildrenData[i].Level = product.Source.ChildrenData[i].Level
	//	category.Source.ChildrenData[i].ProductCount = product.Source.ChildrenData[i].ProductCount
	//	category.Source.ChildrenData[i].ChildrenData = product.Source.ChildrenData[i].ChildrenData
	//	category.Source.ChildrenData[i].Children = product.Source.ChildrenData[i].Children
	//	category.Source.ChildrenData[i].CreatedAt = product.Source.ChildrenData[i].CreatedAt
	//	category.Source.ChildrenData[i].UpdatedAt = product.Source.ChildrenData[i].UpdatedAt
	//	category.Source.ChildrenData[i].Path = product.Source.ChildrenData[i].Path
	//	category.Source.ChildrenData[i].AvailableSortBy = product.Source.ChildrenData[i].AvailableSortBy
	//	category.Source.ChildrenData[i].IncludeInMenu = product.Source.ChildrenData[i].IncludeInMenu
	//	category.Source.ChildrenData[i].IsAnchor = product.Source.ChildrenData[i].IsAnchor
	//	category.Source.ChildrenData[i].ChildrenCount = product.Source.ChildrenData[i].ChildrenCount
	//	category.Source.ChildrenData[i].URLKey = product.Source.ChildrenData[i].URLKey
	//	category.Source.ChildrenData[i].URLPath = product.Source.ChildrenData[i].URLPath
	//}

	//for range product.Source.ChildrenData {
	//	//product.Source.ChildrenData[i].TypeOf = "children_data"
	//	//category.Source.ChildDocuments = append(category.Source.ChildDocuments, product.Source.ChildrenData[i])
	//	FormatChildrenData(product.Source.ChildrenData)
	//}

	return category
}

func FromProductToAttribute(product solr.Product) solr.AttributeStruct {
	var attribute solr.AttributeStruct

	attribute.Index = product.Index
	attribute.Type = product.Type
	attribute.ID = product.ID + "_" + product.Type
	attribute.Score = product.Score

	//_source
	attribute.Source.IsWysiwygEnabled = product.Source.IsWysiwygEnabled
	attribute.Source.IsHTMLAllowedOnFront = product.Source.IsHTMLAllowedOnFront
	attribute.Source.UsedForSortBy = product.Source.UsedForSortBy
	attribute.Source.IsFilterable = product.Source.IsFilterable
	attribute.Source.IsFilterableInSearch = product.Source.IsFilterableInSearch
	attribute.Source.IsUsedInGrid = product.Source.IsUsedInGrid
	attribute.Source.IsVisibleInGrid = product.Source.IsVisibleInGrid
	attribute.Source.IsFilterableInGrid = product.Source.IsFilterableInGrid
	attribute.Source.Position = product.Source.Position
	attribute.Source.ApplyTo = product.Source.ApplyTo
	attribute.Source.IsSearchable = product.Source.IsSearchable
	attribute.Source.IsVisibleInAdvancedSearch = product.Source.IsVisibleInAdvancedSearch
	attribute.Source.IsComparable = product.Source.IsComparable
	attribute.Source.IsUsedForPromoRules = product.Source.IsUsedForPromoRules
	attribute.Source.IsVisibleOnFront = product.Source.IsVisibleOnFront
	attribute.Source.UsedInProductListing = product.Source.UsedInProductListing
	attribute.Source.IsVisible = product.Source.IsVisible
	attribute.Source.Scope = product.Source.Scope
	attribute.Source.AttributeID = product.Source.AttributeID
	attribute.Source.AttributeCode = product.Source.AttributeCode
	attribute.Source.FrontendInput = product.Source.FrontendInput
	attribute.Source.EntityTypeID = product.Source.EntityTypeID
	attribute.Source.IsRequired = product.Source.IsRequired
	attribute.Source.IsUserDefined = product.Source.IsUserDefined
	attribute.Source.DefaultFrontendLabel = product.Source.DefaultFrontendLabel
	attribute.Source.FrontendLabels = product.Source.FrontendLabels
	attribute.Source.BackendType = product.Source.BackendType
	attribute.Source.BackendModel = product.Source.BackendModel
	attribute.Source.SourceModel = product.Source.SourceModel
	attribute.Source.DefaultValue = product.Source.DefaultValue
	attribute.Source.IsUnique = product.Source.IsUnique
	attribute.Source.ValidationRules = product.Source.ValidationRules
	attribute.Source.ID = product.Source.ID
	attribute.Source.Tsk = product.Source.Tsk

	//options
	for i := range product.Source.Options {
		product.Source.Options[i].TypeOf = "options"
		product.Source.Options[i].ParentID = product.Source.ID
		attribute.Source.ChildDocuments = append(attribute.Source.ChildDocuments, product.Source.Options[i])
	}

	return attribute
}

func FromProductToTaxRules(product solr.Product) solr.TaxRuleStruct {
	var taxRules solr.TaxRuleStruct

	taxRules.Index = product.Index
	taxRules.Type = product.Type
	taxRules.ID = product.ID + "_" + product.Type
	taxRules.Score = product.Score

	//_source
	taxRules.Source.ID = product.Source.ID
	taxRules.Source.Code = product.Source.Code
	taxRules.Source.Priority = product.Source.Priority
	taxRules.Source.Position = product.Source.Position
	taxRules.Source.CustomerTaxClassIds = product.Source.CustomerTaxClassIds
	taxRules.Source.ProductTaxClassIds = product.Source.ProductTaxClassIds
	taxRules.Source.TaxRateIds = product.Source.TaxRateIds
	taxRules.Source.CalculateSubtotal = product.Source.CalculateSubtotal
	taxRules.Source.Tsk = product.Source.Tsk

	//rates
	for i := range product.Source.Rates {
		product.Source.Rates[i].TypeOf = "rates"
		product.Source.Rates[i].ParentID = product.Source.ID

		taxRules.Source.ChildDocuments = append(taxRules.Source.ChildDocuments, product.Source.Rates[i])
	}

	return taxRules
}

func ChangeStructs(product []solr.Product) solr.SolrDataStruct {
	var solrDatStruct solr.SolrDataStruct
	for i := range product {
		if product[i].Type == "product" {
			if product[i].Source.TypeID == "simple" {
				solrDatStruct.SimpleProductStruct = append(solrDatStruct.SimpleProductStruct, FromProductToSimpleProduct(product[i]))
			} else {
				solrDatStruct.ConfigurableProductStruct = append(solrDatStruct.ConfigurableProductStruct, FromProductToConfigurableProduct(product[i]))
			}
		} else if product[i].Type == "category" {
			solrDatStruct.CategoryStruct = append(solrDatStruct.CategoryStruct, FromProductToCategory(product[i]))
		} else if product[i].Type == "attribute" {
			solrDatStruct.AttributeStruct = append(solrDatStruct.AttributeStruct, FromProductToAttribute(product[i]))
		} else {
			solrDatStruct.TaxRuleStruct = append(solrDatStruct.TaxRuleStruct, FromProductToTaxRules(product[i]))
		}
	}
	//catUrl := "/Users/marius/go/src/sway-commerce-admin-api/categories.json"
	//category := category.ChangeStructToVue(category.LoadCategoriesFromFile(catUrl))
	//solrDatStruct.CategoryStruct = category
	return solrDatStruct
}
