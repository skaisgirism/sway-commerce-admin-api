package solrProduct

import (
	"github.com/go-chi/chi"
	"net/http"
)

func RouterSolrProduct() http.Handler {
	r := chi.NewRouter()
	r.Post("/", productDataPushToSolr)
	return r
}
