package solrProduct

import (
	"bitbucket.org/sway-commerce/sway-commerce-admin-api/helpers"
	"bitbucket.org/sway-commerce/sway-commerce-admin-api/solr"
	"fmt"
	"github.com/shopspring/decimal"
	"strconv"
)

func NewProduct() solr.Product {
	var defaultVueProduct solr.Product

	defaultVueProduct.Index = "vue_storefront_catalog_1530106022"
	defaultVueProduct.Type = "product"
	defaultVueProduct.Score = 1
	defaultVueProduct.Source.Status = 1
	defaultVueProduct.Source.Visibility = 4
	defaultVueProduct.Source.Stock.IsQtyDecimal = false
	defaultVueProduct.Source.Stock.ShowDefaultNotificationMessage = false
	defaultVueProduct.Source.Stock.UseConfigMinQty = true
	defaultVueProduct.Source.Stock.MinQty = 0
	defaultVueProduct.Source.Stock.UseConfigMinSaleQty = 1
	defaultVueProduct.Source.Stock.MinSaleQty = 1
	defaultVueProduct.Source.Stock.UseConfigMaxSaleQty = true
	defaultVueProduct.Source.Stock.MaxSaleQty = 10000
	defaultVueProduct.Source.Stock.UseConfigBackorders = true
	defaultVueProduct.Source.Stock.Backorders = 0
	defaultVueProduct.Source.Stock.UseConfigNotifyStockQty = true
	defaultVueProduct.Source.Stock.NotifyStockQty = 1
	defaultVueProduct.Source.Stock.UseConfigQtyIncrements = true
	defaultVueProduct.Source.Stock.QtyIncrements = 0
	defaultVueProduct.Source.Stock.UseConfigEnableQtyInc = true
	defaultVueProduct.Source.Stock.EnableQtyIncrements = false
	defaultVueProduct.Source.Stock.UseConfigManageStock = true
	defaultVueProduct.Source.Stock.ManageStock = true
	defaultVueProduct.Source.Stock.IsDecimalDivided = false
	defaultVueProduct.Source.Stock.StockStatusChangedAuto = 0
	defaultVueProduct.Source.OptionsContainer = "container2"
	defaultVueProduct.Source.RequiredOptions = "0"
	defaultVueProduct.Source.HasOptions = "0"
	defaultVueProduct.Source.TaxClassID = "2"

	return defaultVueProduct
}

//gets int from string
func extractInt(value string) int {
	result, err := strconv.Atoi(value)
	helpers.CheckErr(err)
	return result
}

//Gets price/decimal from string
func extractPrice(value string) decimal.Decimal {
	result, err := decimal.NewFromString(value)
	helpers.CheckErr(err)
	return result
}

//Gets float from string
func extractFloat(value string) float64 {
	result, err := strconv.ParseFloat(value, 64)
	helpers.CheckErr(err)
	return result
}

//get boolean from string
func extractBool(value interface{}) bool {
	var result bool
	if value == "1" {
		result = true
	}
	if value == "0" {
		result = false
	} else {
		fmt.Errorf("Wrong value for extractBool")
	}
	return result
}
