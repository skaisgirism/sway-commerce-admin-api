package solrCategory

import "net/http"

//Salon Pro category URL
const CategoryURL = "https://www.salonpro.lt/export/index/category/"
const path = "/Users/marius/go/src/sway-commerce-admin-api/categories.json"


func categoryDataPushToSolr(w http.ResponseWriter, r *http.Request) {
	salonCategoryList := LoadCategoriesFromFile(path)

	vueCategory := ChangeStructToVue(salonCategoryList)

	PushCategoriesToSolr(vueCategory)
}
