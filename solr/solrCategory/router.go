package solrCategory

import (
	"github.com/go-chi/chi"
	"net/http"
)

func RouterSolrCategory() http.Handler {
	r := chi.NewRouter()
	r.Post("/", categoryDataPushToSolr)
	return r
}
