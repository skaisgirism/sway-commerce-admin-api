package solrCategory

import (
	"strings"
)

//func remap(categories []*salonCategory){
//	categoryChild := make (map[string][]string, 0)
//	for i, v := range categories {
//		categoryChild[v.Id] := make([]string)
//		categoryChild[v.Id] := append(categoryChild[v.Id], vchild.id)
//
//	}
//
//}

func makeCategoriesWithParents(cats []*salonCategory) []*salonCategory {
	catsMap := getCategoryMap(cats)

	for _, cat := range cats {
		hasChildren := len(catsMap[cat.EntityID]) > 0
		if hasChildren {
			cat.Children = strings.Join(catsMap[cat.EntityID], ",")
		}
	}
	return cats
}

func getCategoryMap(cats []*salonCategory) map[string][]string {
	m := make(map[string][]string)
	for _, cat := range cats {
		l := m[cat.ParentID]
		if l == nil {
			l = make([]string, 0)
		}
		l = append(l, cat.EntityID)
		m[cat.ParentID] = l
	}
	return m
}
