package solrCategory

import (
	"testing"
)

func Test_loadCategoryDataFromUrl(t *testing.T) {
	catUrl := "https://www.salonpro.lt/export/index/category"
	salCategories, err := LoadCategoryDataFromUrl(catUrl)
	if err != nil {
		t.Error(err)
	}

	if len(salCategories) == 0 {
		t.Errorf("Empty list of categories returned")
	}
}

func Test_loadCategoriesFromFile(t *testing.T) {
	path := "/Users/remis/work/sway/sway-commerce-admin-api/categories.json"
	cats, err := LoadCategoriesFromFile(path)
	if err != nil {
		t.Error(err)
	}

	if len(cats) == 0 {
		t.Errorf("Empty list of categories returned")
	}
}
