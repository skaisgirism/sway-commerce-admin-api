package solrCategory

import (
	"bitbucket.org/sway-commerce/sway-commerce-admin-api/helpers"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

func LoadCategoriesFromFile(file string) []*salonCategory {
	// Open our jsonFile
	jsonFile, err := os.Open(file)
	// if we os.Open returns an error then handle it
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("Successfully Opened users.json")
	// defer the closing of our jsonFile so that we can parse it later on
	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)
	salonCategoryList := make([]*salonCategory, 0)
	json.Unmarshal(byteValue, &salonCategoryList)
	if err != nil && err.Error() != "EOF" {
		return nil
	}
	return salonCategoryList
}

//Decodes body from URL and returns a SalonPro category array
func LoadCategoryDataFromUrl(url string) ([]salonCategory, error) {
	resp, err := http.Get(url)
	helpers.CheckErr(err)
	defer resp.Body.Close()

	salonCategoryList := make([]salonCategory, 0)
	err = json.NewDecoder(resp.Body).Decode(&salonCategoryList)
	if err != nil && err.Error() != "EOF" {
		return nil, err
	}
	return salonCategoryList, nil
}

//Maps salonCategory to vueCategory
func ChangeStructToVue(categoryList []*salonCategory) []vueTestCategory {
	salonCategoryList := makeCategoriesWithParents(categoryList)
	var category vueTestCategory
	vueCategoryList := make([]vueTestCategory, 0)

	for i := range salonCategoryList {
		category.Index = "vue_storefront_catalog_1556379297"
		category.Type = "category"
		category.ID = salonCategoryList[i].EntityID
		category.Score = 1

		//_source
		//functionality for AvailableSortBy field not added
		//category.Source.ID = helpers.ExtractInt(salonCategoryList[i].EntityID)
		category.ParentID = helpers.ExtractInt(salonCategoryList[i].ParentID)
		category.Name = salonCategoryList[i].Name

		if salonCategoryList[i].IsActive == "1" {
			category.IsActive = true
		} else {
			category.IsActive = false
		}
		category.Position = helpers.ExtractInt(salonCategoryList[i].Position)

		//lowering nested levels to 4
		if helpers.ExtractInt(salonCategoryList[i].Level) >= 4 {
			category.Level = 4
		}else{
			category.Level = helpers.ExtractInt(salonCategoryList[i].Level)
		}

		category.ProductCount = 0
		category.Children = salonCategoryList[i].Children

		category.CreatedAt = salonCategoryList[i].CreatedAt
		category.UpdatedAt = salonCategoryList[i].UpdatedAt
		category.Path = salonCategoryList[i].Path

		if salonCategoryList[i].IncludeInMenu == "1" {
			category.IncludeInMenu = true
		} else {
			category.IncludeInMenu = false
		}

		category.IsAnchor = salonCategoryList[i].IsAnchor
		category.ChildrenCount = salonCategoryList[i].ChildrenCount
		category.URLKey = salonCategoryList[i].URLKey
		category.URLPath = salonCategoryList[i].URLPath
		category.Tsk = 1556379380757

		vueCategoryList = append(vueCategoryList, category)
	}
	return vueCategoryList
}
func PushCategoriesToSolr(data []vueTestCategory) {
	//Makes copy of struct to Buffer
	structCopy := new(bytes.Buffer)
	//Encodes it
	json.NewEncoder(structCopy).Encode(data)
	//Sends encoded structCopy to Solr search engine
	_, err := http.Post("http://192.168.0.2:8983/solr/storefrontCore/update/json/docs?"+
		"&f=/**"+
		"&commit=true", "application/json; charset=utf-8", structCopy)
	helpers.CheckErr(err)

}
