package solrCategory

import (
	"fmt"
	"github.com/labstack/gommon/log"
	"testing"
)

func Test_remap(t *testing.T) {

	catUrl := "/Users/remis/work/sway/sway-commerce-admin-api/categories.json"
	cats, err := LoadCategoriesFromFile(catUrl)
	if err != nil {
		t.Error(err)
	}

	m := make(map[string][]string)
	for _, cat := range cats {
		l := m[cat.ParentID]
		if l == nil {
			l = make([]string, 0)
		}
		l = append(l, cat.EntityID)
		m[cat.ParentID] = l
	}

	fmt.Printf("%+v", m)
}

func Test_remap1(t *testing.T) {

}

func Test_makeCategoriesWithParents(t *testing.T) {
	catUrl := "/Users/remis/work/sway/sway-commerce-admin-api/categories.json"
	cats, err := LoadCategoriesFromFile(catUrl)
	if err != nil {
		log.Error(err)
	}
	catsWithParent := makeCategoriesWithParents(cats)
	if len(catsWithParent) == 0 {
		t.Error("Adding parent to categories has failed")
	}
}
