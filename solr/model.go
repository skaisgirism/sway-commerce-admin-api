package solr

import (
	"encoding/json"
)

type SimpleProductStruct struct {
	Index   string `json:"_index"`
	Type    string `json:"_type"`
	ID      string `json:"_id"`
	Score   int    `json:"_score"`
	DocType string `json:"doc_type"`
	Source  struct {
		ID                  int         `json:"id"`
		Sku                 string      `json:"sku"`
		Name                string      `json:"name"`
		AttributeSetID      int         `json:"attribute_set_id"`
		Price               float64     `json:"price"`
		Status              int         `json:"status"`
		Visibility          int         `json:"visibility"`
		TypeID              string      `json:"type_id"`
		CreatedAt           string      `json:"created_at"`
		UpdatedAt           string      `json:"updated_at"`
		CustomAttributes    interface{} `json:"custom_attributes"`
		FinalPrice          float64     `json:"final_price"`
		MaxPrice            float64     `json:"max_price"`
		MaxRegularPrice     float64     `json:"max_regular_price"`
		MinimalRegularPrice float64     `json:"minimal_regular_price"`
		MinimalPrice        float64     `json:"minimal_price"`
		RegularPrice        float64     `json:"regular_price"`
		Stock               struct {
			ItemID                         int         `json:"item_id"`
			ProductID                      int         `json:"product_id"`
			StockID                        int         `json:"stock_id"`
			Qty                            int         `json:"qty"`
			IsInStock                      bool        `json:"is_in_stock"`
			IsQtyDecimal                   bool        `json:"is_qty_decimal"`
			ShowDefaultNotificationMessage bool        `json:"show_default_notification_message"`
			UseConfigMinQty                bool        `json:"use_config_min_qty"`
			MinQty                         int         `json:"min_qty"`
			UseConfigMinSaleQty            int         `json:"use_config_min_sale_qty"`
			MinSaleQty                     int         `json:"min_sale_qty"`
			UseConfigMaxSaleQty            bool        `json:"use_config_max_sale_qty"`
			MaxSaleQty                     int         `json:"max_sale_qty"`
			UseConfigBackorders            bool        `json:"use_config_backorders"`
			Backorders                     int         `json:"backorders"`
			UseConfigNotifyStockQty        bool        `json:"use_config_notify_stock_qty"`
			NotifyStockQty                 int         `json:"notify_stock_qty"`
			UseConfigQtyIncrements         bool        `json:"use_config_qty_increments"`
			QtyIncrements                  int         `json:"qty_increments"`
			UseConfigEnableQtyInc          bool        `json:"use_config_enable_qty_inc"`
			EnableQtyIncrements            bool        `json:"enable_qty_increments"`
			UseConfigManageStock           bool        `json:"use_config_manage_stock"`
			ManageStock                    bool        `json:"manage_stock"`
			LowStockDate                   interface{} `json:"low_stock_date"`
			IsDecimalDivided               bool        `json:"is_decimal_divided"`
			StockStatusChangedAuto         int         `json:"stock_status_changed_auto"`
		} `json:"stock"`
		Tsk              int64         `json:"tsk"`
		Description      string        `json:"description"`
		Image            string        `json:"image"`
		SmallImage       string        `json:"small_image"`
		Thumbnail        string        `json:"thumbnail"`
		CategoryIds      []string      `json:"category_ids"`
		OptionsContainer string        `json:"options_container"`
		RequiredOptions  string        `json:"required_options"`
		HasOptions       string        `json:"has_options"`
		URLKey           string        `json:"url_key"`
		TaxClassID       string        `json:"tax_class_id"`
		Activity         string        `json:"activity,omitempty"`
		Material         string        `json:"material,omitempty"`
		Gender           string        `json:"gender"`
		CategoryGear     string        `json:"category_gear"`
		ErinRecommends   string        `json:"erin_recommends"`
		New              string        `json:"new"`
		ChildDocuments   []interface{} `json:"_childDocuments_"`
	} `json:"_source"`
}

type ConfigurableProductStruct struct {
	Index   string `json:"_index"`
	Type    string `json:"_type"`
	ID      string `json:"_id"`
	Score   int    `json:"_score"`
	DocType string `json:"doc_type"`
	Source  struct {
		ID                  int         `json:"id"`
		Sku                 string      `json:"sku"`
		Name                string      `json:"name"`
		AttributeSetID      int         `json:"attribute_set_id"`
		Price               float64     `json:"price"`
		Status              int         `json:"status"`
		Visibility          int         `json:"visibility"`
		TypeID              string      `json:"type_id"`
		CreatedAt           string      `json:"created_at"`
		UpdatedAt           string      `json:"updated_at"`
		CustomAttributes    interface{} `json:"custom_attributes"`
		FinalPrice          float64     `json:"final_price"`
		MaxPrice            float64     `json:"max_price"`
		MaxRegularPrice     float64     `json:"max_regular_price"`
		MinimalRegularPrice float64     `json:"minimal_regular_price"`
		MinimalPrice        float64     `json:"minimal_price"`
		RegularPrice        float64     `json:"regular_price"`
		Stock               struct {
			ItemID                         int         `json:"item_id"`
			ProductID                      int         `json:"product_id"`
			StockID                        int         `json:"stock_id"`
			Qty                            int         `json:"qty"`
			IsInStock                      bool        `json:"is_in_stock"`
			IsQtyDecimal                   bool        `json:"is_qty_decimal"`
			ShowDefaultNotificationMessage bool        `json:"show_default_notification_message"`
			UseConfigMinQty                bool        `json:"use_config_min_qty"`
			MinQty                         int         `json:"min_qty"`
			UseConfigMinSaleQty            int         `json:"use_config_min_sale_qty"`
			MinSaleQty                     int         `json:"min_sale_qty"`
			UseConfigMaxSaleQty            bool        `json:"use_config_max_sale_qty"`
			MaxSaleQty                     int         `json:"max_sale_qty"`
			UseConfigBackorders            bool        `json:"use_config_backorders"`
			Backorders                     int         `json:"backorders"`
			UseConfigNotifyStockQty        bool        `json:"use_config_notify_stock_qty"`
			NotifyStockQty                 int         `json:"notify_stock_qty"`
			UseConfigQtyIncrements         bool        `json:"use_config_qty_increments"`
			QtyIncrements                  int         `json:"qty_increments"`
			UseConfigEnableQtyInc          bool        `json:"use_config_enable_qty_inc"`
			EnableQtyIncrements            bool        `json:"enable_qty_increments"`
			UseConfigManageStock           bool        `json:"use_config_manage_stock"`
			ManageStock                    bool        `json:"manage_stock"`
			LowStockDate                   interface{} `json:"low_stock_date"`
			IsDecimalDivided               bool        `json:"is_decimal_divided"`
			StockStatusChangedAuto         int         `json:"stock_status_changed_auto"`
		} `json:"stock"`
		ColorOptions               []int         `json:"color_options"`
		SizeOptions                []int         `json:"size_options"`
		Tsk                        int64         `json:"tsk"`
		Description                string        `json:"description"`
		Image                      string        `json:"image"`
		SmallImage                 string        `json:"small_image"`
		Thumbnail                  string        `json:"thumbnail"`
		CategoryIds                []string      `json:"category_ids"`
		OptionsContainer           string        `json:"options_container"`
		RequiredOptions            string        `json:"required_options"`
		HasOptions                 string        `json:"has_options"`
		URLKey                     string        `json:"url_key"`
		MsrpDisplayActualPriceType string        `json:"msrp_display_actual_price_type"`
		TaxClassID                 string        `json:"tax_class_id,omitempty"`
		Material                   string        `json:"material"`
		EcoCollection              string        `json:"eco_collection"`
		PerformanceFabric          string        `json:"performance_fabric"`
		ErinRecommends             string        `json:"erin_recommends"`
		New                        string        `json:"new"`
		Sale                       string        `json:"sale"`
		Pattern                    string        `json:"pattern"`
		Climate                    string        `json:"climate"`
		ChildDocuments             []interface{} `json:"_childDocuments_"`
	} `json:"_source"`
}

type CategoryStruct struct {
	Index  string `json:"_index"`
	Type   string `json:"_type"`
	ID     string `json:"_id"`
	Score  int    `json:"_score"`
	Source struct {
		ID           int    `json:"id"`
		ParentID     int    `json:"parent_id"`
		Name         string `json:"name"`
		IsActive     bool   `json:"is_active"`
		Position     int    `json:"position"`
		Level        int    `json:"level"`
		ProductCount int    `json:"product_count"`
		//ChildrenData    []ChildrenData `json:"children_data"`
		Children        string        `json:"children"`
		CreatedAt       string        `json:"created_at"`
		UpdatedAt       string        `json:"updated_at"`
		Path            string        `json:"path"`
		AvailableSortBy []interface{} `json:"available_sort_by"`
		IncludeInMenu   bool          `json:"include_in_menu"`
		IsAnchor        string        `json:"is_anchor"`
		ChildrenCount   string        `json:"children_count"`
		URLKey          string        `json:"url_key"`
		URLPath         string        `json:"url_path"`
		Tsk             int64         `json:"tsk"`
		ChildDocuments  []interface{} `json:"_childDocuments_"`
	} `json:"_source"`
}

type AttributeStruct struct {
	Index  string `json:"_index"`
	Type   string `json:"_type"`
	ID     string `json:"_id"`
	Score  int    `json:"_score"`
	Source struct {
		IsWysiwygEnabled          bool          `json:"is_wysiwyg_enabled"`
		IsHTMLAllowedOnFront      bool          `json:"is_html_allowed_on_front"`
		UsedForSortBy             bool          `json:"used_for_sort_by"`
		IsFilterable              bool          `json:"is_filterable"`
		IsFilterableInSearch      bool          `json:"is_filterable_in_search"`
		IsUsedInGrid              bool          `json:"is_used_in_grid"`
		IsVisibleInGrid           bool          `json:"is_visible_in_grid"`
		IsFilterableInGrid        bool          `json:"is_filterable_in_grid"`
		Position                  int           `json:"position"`
		ApplyTo                   []interface{} `json:"apply_to"`
		IsSearchable              string        `json:"is_searchable"`
		IsVisibleInAdvancedSearch string        `json:"is_visible_in_advanced_search"`
		IsComparable              string        `json:"is_comparable"`
		IsUsedForPromoRules       string        `json:"is_used_for_promo_rules"`
		IsVisibleOnFront          string        `json:"is_visible_on_front"`
		UsedInProductListing      string        `json:"used_in_product_listing"`
		IsVisible                 bool          `json:"is_visible"`
		Scope                     string        `json:"scope"`
		AttributeID               int           `json:"attribute_id"`
		AttributeCode             string        `json:"attribute_code"`
		FrontendInput             string        `json:"frontend_input"`
		EntityTypeID              string        `json:"entity_type_id"`
		IsRequired                bool          `json:"is_required"`
		IsUserDefined             bool          `json:"is_user_defined"`
		DefaultFrontendLabel      string        `json:"default_frontend_label"`
		FrontendLabels            interface{}   `json:"frontend_labels"`
		BackendType               string        `json:"backend_type"`
		BackendModel              string        `json:"backend_model"`
		SourceModel               string        `json:"source_model"`
		DefaultValue              string        `json:"default_value"`
		IsUnique                  string        `json:"is_unique"`
		ValidationRules           []interface{} `json:"validation_rules"`
		ID                        int           `json:"id"`
		Tsk                       int64         `json:"tsk"`
		ChildDocuments            []interface{} `json:"_childDocuments_"`
	} `json:"_source"`
}

type TaxRuleStruct struct {
	Index  string `json:"_index"`
	Type   string `json:"_type"`
	ID     string `json:"_id"`
	Score  int    `json:"_score"`
	Source struct {
		ID                  int           `json:"id"`
		Code                string        `json:"code"`
		Priority            int           `json:"priority"`
		Position            int           `json:"position"`
		CustomerTaxClassIds []int         `json:"customer_tax_class_ids"`
		ProductTaxClassIds  []int         `json:"product_tax_class_ids"`
		TaxRateIds          []int         `json:"tax_rate_ids"`
		CalculateSubtotal   bool          `json:"calculate_subtotal"`
		Tsk                 int64         `json:"tsk"`
		ChildDocuments      []interface{} `json:"_childDocuments_"`
	} `json:"_source"`
}

type SolrDataStruct struct {
	SimpleProductStruct       []SimpleProductStruct
	ConfigurableProductStruct []ConfigurableProductStruct
	CategoryStruct            []CategoryStruct
	AttributeStruct           []AttributeStruct
	TaxRuleStruct             []TaxRuleStruct
}

type Product struct {
	Index  string `json:"_index"`
	Type   string `json:"_type"`
	ID     string `json:"_id"`
	Score  int    `json:"_score"`
	Source Source `json:"_source"`
}

type Source struct {
	TypeOf                     string                 `json:"type_of,omitempty"`
	ParentId                   int                    `json:"parent_id,omitempty"`
	ID                         int                    `json:"id,omitempty"`
	Sku                        string                 `json:"sku,omitempty"`
	Name                       string                 `json:"name,omitempty"`
	AttributeSetID             int                    `json:"attribute_set_id,omitempty"`
	Price                      float64                `json:"price,omitempty"`
	Status                     int                    `json:"status,omitempty"`
	Visibility                 int                    `json:"visibility,omitempty"`
	TypeID                     string                 `json:"type_id,omitempty"`
	CreatedAt                  string                 `json:"created_at,omitempty"`
	UpdatedAt                  string                 `json:"updated_at,omitempty"`
	Weight                     int                    `json:"weight,omitempty"`
	ProductLinks               []ProductLinks         `json:"product_links,omitempty"`
	TierPrices                 []TierPrices           `json:"tier_prices,omitempty"`
	CustomAttributes           interface{}            `json:"custom_attributes,omitempty"`
	FinalPrice                 float64                `json:"final_price,omitempty"`
	MaxPrice                   float64                `json:"max_price,omitempty"`
	MaxRegularPrice            float64                `json:"max_regular_price,omitempty"`
	MinimalRegularPrice        float64                `json:"minimal_regular_price,omitempty"`
	MinimalPrice               float64                `json:"minimal_price,omitempty"`
	RegularPrice               float64                `json:"regular_price,omitempty"`
	Category                   []Category             `json:"category,omitempty"`
	Tsk                        int64                  `json:"tsk,omitempty"`
	Description                string                 `json:"description,omitempty"`
	Image                      string                 `json:"image,omitempty"`
	SmallImage                 string                 `json:"small_image,omitempty"`
	Thumbnail                  string                 `json:"thumbnail,omitempty"`
	Color                      *string                `json:"color,omitempty"`
	CategoryIds                []string               `json:"category_ids,omitempty"`
	OptionsContainer           string                 `json:"options_container,omitempty"`
	RequiredOptions            string                 `json:"required_options,omitempty"`
	HasOptions                 string                 `json:"has_options,omitempty"`
	URLKey                     string                 `json:"url_key,omitempty"`
	MsrpDisplayActualPriceType string                 `json:"msrp_display_actual_price_type,omitempty"`
	TaxClassID                 string                 `json:"tax_class_id,omitempty"`
	Size                       string                 `json:"size,omitempty"`
	Stock                      Stock                  `json:"stock,omitempty"`
	MediaGallery               []MediaGallery         `json:"media_gallery,omitempty"`
	ConfigurableChildren       []ConfigurableChildren `json:"configurable_children,omitempty"`
	ConfigurableOptions        []ConfigurableOptions  `json:"configurable_options,omitempty"`
	ColorOptions               []int                  `json:"color_options,omitempty"`
	SizeOptions                []int                  `json:"size_options,omitempty"`
	Material                   string                 `json:"material,omitempty"`
	EcoCollection              string                 `json:"eco_collection,omitempty"`
	PerformanceFabric          string                 `json:"performance_fabric,omitempty"`
	ErinRecommends             string                 `json:"erin_recommends,omitempty"`
	New                        string                 `json:"new,omitempty"`
	Sale                       string                 `json:"sale,omitempty"`
	Pattern                    string                 `json:"pattern,omitempty"`
	Climate                    string                 `json:"climate,omitempty"`
	IsActive                   bool                   `json:"is_active,omitempty"`
	Position                   int                    `json:"position,omitempty"`
	Level                      int                    `json:"level,omitempty"`
	ProductCount               int                    `json:"product_count,omitempty"`
	Children                   string                 `json:"children,omitempty"`
	Path                       string                 `json:"path,omitempty"`
	AvailableSortBy            []interface{}          `json:"available_sort_by,omitempty"`
	IncludeInMenu              bool                   `json:"include_in_menu,omitempty"`
	IsAnchor                   string                 `json:"is_anchor,omitempty"`
	ChildrenCount              string                 `json:"children_count,omitempty"`
	URLPath                    string                 `json:"url_path,omitempty"`
	ChildrenData               []ChildrenData         `json:"children_data,omitempty"`
	IsWysiwygEnabled           bool                   `json:"is_wysiwyg_enabled,omitempty"`
	IsHTMLAllowedOnFront       bool                   `json:"is_html_allowed_on_front,omitempty"`
	UsedForSortBy              bool                   `json:"used_for_sort_by,omitempty"`
	IsFilterable               bool                   `json:"is_filterable,omitempty"`
	IsFilterableInSearch       bool                   `json:"is_filterable_in_search,omitempty"`
	IsUsedInGrid               bool                   `json:"is_used_in_grid,omitempty"`
	IsVisibleInGrid            bool                   `json:"is_visible_in_grid,omitempty"`
	IsFilterableInGrid         bool                   `json:"is_filterable_in_grid,omitempty"`
	ApplyTo                    []interface{}          `json:"apply_to,omitempty"`
	IsSearchable               string                 `json:"is_searchable,omitempty"`
	IsVisibleInAdvancedSearch  string                 `json:"is_visible_in_advanced_search,omitempty"`
	IsComparable               string                 `json:"is_comparable,omitempty"`
	IsUsedForPromoRules        string                 `json:"is_used_for_promo_rules,omitempty"`
	IsVisibleOnFront           string                 `json:"is_visible_on_front,omitempty"`
	UsedInProductListing       string                 `json:"used_in_product_listing,omitempty"`
	IsVisible                  bool                   `json:"is_visible,omitempty"`
	Scope                      string                 `json:"scope,omitempty"`
	AttributeID                int                    `json:"attribute_id,omitempty"`
	AttributeCode              string                 `json:"attribute_code,omitempty"`
	FrontendInput              string                 `json:"frontend_input,omitempty"`
	EntityTypeID               string                 `json:"entity_type_id,omitempty"`
	IsRequired                 bool                   `json:"is_required,omitempty"`
	Options                    []Options              `json:"options,omitempty"`
	IsUserDefined              bool                   `json:"is_user_defined,omitempty"`
	DefaultFrontendLabel       string                 `json:"default_frontend_label,omitempty"`
	FrontendLabels             interface{}            `json:"frontend_labels,omitempty"`
	BackendType                string                 `json:"backend_type,omitempty"`
	BackendModel               string                 `json:"backend_model,omitempty"`
	SourceModel                string                 `json:"source_model,omitempty"`
	DefaultValue               string                 `json:"default_value,omitempty"`
	IsUnique                   string                 `json:"is_unique,omitempty"`
	ValidationRules            []interface{}          `json:"validation_rules,omitempty"`
	Code                       string                 `json:"code,omitempty"`
	Priority                   int                    `json:"priority,omitempty"`
	CustomerTaxClassIds        []int                  `json:"customer_tax_class_ids,omitempty"`
	ProductTaxClassIds         []int                  `json:"product_tax_class_ids,omitempty"`
	TaxRateIds                 []int                  `json:"tax_rate_ids,omitempty"`
	CalculateSubtotal          bool                   `json:"calculate_subtotal,omitempty"`
	Rates                      []Rates                `json:"rates,omitempty"`
	Activity                   string                 `json:"activity,omitempty"`
	Gender                     string                 `json:"gender,omitempty"`
	CategoryGear               string                 `json:"category_gear,omitempty"`
}

type Stock struct {
	TypeOf                         string      `json:"type_of,omitempty"`
	ParentId                       int         `json:"parent_id,omitempty"`
	ItemID                         int         `json:"item_id"`
	ProductID                      int         `json:"product_id"`
	StockID                        int         `json:"stock_id"`
	Qty                            int         `json:"qty"`
	IsInStock                      bool        `json:"is_in_stock"`
	IsQtyDecimal                   bool        `json:"is_qty_decimal"`
	ShowDefaultNotificationMessage bool        `json:"show_default_notification_message"`
	UseConfigMinQty                bool        `json:"use_config_min_qty"`
	MinQty                         int         `json:"min_qty"`
	UseConfigMinSaleQty            int         `json:"use_config_min_sale_qty"`
	MinSaleQty                     int         `json:"min_sale_qty"`
	UseConfigMaxSaleQty            bool        `json:"use_config_max_sale_qty"`
	MaxSaleQty                     int         `json:"max_sale_qty"`
	UseConfigBackorders            bool        `json:"use_config_backorders"`
	Backorders                     int         `json:"backorders"`
	UseConfigNotifyStockQty        bool        `json:"use_config_notify_stock_qty"`
	NotifyStockQty                 int         `json:"notify_stock_qty"`
	UseConfigQtyIncrements         bool        `json:"use_config_qty_increments"`
	QtyIncrements                  int         `json:"qty_increments"`
	UseConfigEnableQtyInc          bool        `json:"use_config_enable_qty_inc"`
	EnableQtyIncrements            bool        `json:"enable_qty_increments"`
	UseConfigManageStock           bool        `json:"use_config_manage_stock"`
	ManageStock                    bool        `json:"manage_stock"`
	LowStockDate                   interface{} `json:"low_stock_date"`
	IsDecimalDivided               bool        `json:"is_decimal_divided"`
	StockStatusChangedAuto         int         `json:"stock_status_changed_auto"`
}

type ChildrenData struct {
	TypeOf          string        `json:"type_of"`
	ID              int           `json:"id"`
	ParentID        int           `json:"parent_id"`
	Name            string        `json:"name"`
	IsActive        bool          `json:"is_active"`
	Position        int           `json:"position"`
	Level           int           `json:"level"`
	ProductCount    int           `json:"product_count"`
	Children        string        `json:"children"`
	CreatedAt       string        `json:"created_at"`
	UpdatedAt       string        `json:"updated_at"`
	Path            string        `json:"path"`
	AvailableSortBy []interface{} `json:"available_sort_by"`
	IncludeInMenu   bool          `json:"include_in_menu"`
	IsAnchor        string        `json:"is_anchor"`
	ChildrenCount   string        `json:"children_count"`
	URLKey          string        `json:"url_key"`
	URLPath         string        `json:"url_path"`
}

type MediaGallery struct {
	TypeOf   string `json:"type_of,omitempty"`
	ParentID int    `json:"parent_id,omitempty"`
	Image    string `json:"image,omitempty"`
	Pos      int    `json:"pos,omitempty"`
	Typ      string `json:"typ,omitempty"`
	Lab      string `json:"lab,omitempty"`
}

type ConfigurableChildren struct {
	TypeOf                     string   `json:"type_of,omitempty"`
	ParentID                   int      `json:"parent_id,omitempty"`
	Sku                        string   `json:"sku"`
	ID                         int      `json:"id"`
	Status                     int      `json:"status"`
	Name                       string   `json:"name"`
	Price                      int      `json:"price"`
	RequiredOptions            string   `json:"required_options"`
	HasOptions                 string   `json:"has_options"`
	TaxClassID                 string   `json:"tax_class_id"`
	CategoryIds                []string `json:"category_ids"`
	Size                       string   `json:"size"`
	Color                      string   `json:"color"`
	Image                      string   `json:"image"`
	SmallImage                 string   `json:"small_image"`
	Thumbnail                  string   `json:"thumbnail"`
	URLKey                     string   `json:"url_key"`
	MsrpDisplayActualPriceType string   `json:"msrp_display_actual_price_type"`
	FinalPrice                 float64  `json:"final_price"`
	MaxPrice                   float64  `json:"max_price"`
	MaxRegularPrice            float64  `json:"max_regular_price"`
	MinimalRegularPrice        float64  `json:"minimal_regular_price"`
	MinimalPrice               float64  `json:"minimal_price"`
	RegularPrice               float64  `json:"regular_price"`
}

type ConfigurableOptions struct {
	TypeOf                string   `json:"type_of,omitempty"`
	ParentID              int      `json:"parent_id,omitempty"`
	ID                    int      `json:"id,omitempty"`
	ConfigurableOptionsId int      `json:"configurable_options_id,omitempty"`
	AttributeID           string   `json:"attribute_id"`
	Label                 string   `json:"label"`
	Position              int      `json:"position"`
	Values                []Values `json:"values"`
	ProductID             int      `json:"product_id"`
	AttributeCode         string   `json:"attribute_code"`
}

type Values struct {
	ValueIndex int `json:"value_index"`
}

type Category struct {
	TypeOf     string `json:"type_of,omitempty"`
	ParentID   int    `json:"parent_id,omitempty"`
	CategoryID int    `json:"category_id"`
	Name       string `json:"name"`
}

type Options struct {
	TypeOf   string `json:"type_of"`
	ParentID int    `json:"parent_id"`
	Label    string `json:"label"`
	Value    string `json:"value"`
}

type Rates struct {
	TypeOf       string        `json:"type_of"`
	ParentID     int           `json:"parent_id"`
	ID           int           `json:"id"`
	TaxCountryID string        `json:"tax_country_id"`
	TaxRegionID  int           `json:"tax_region_id"`
	TaxPostcode  string        `json:"tax_postcode"`
	Rate         int           `json:"rate"`
	Code         string        `json:"code"`
	Titles       []interface{} `json:"titles"`
}

type ProductLinks struct {
	TypeOf            string `json:"type_of,omitempty"`
	ParentID          int    `json:"parent_id"`
	SKU               string `json:"sku"`
	LinkType          string `json:"link_type"`
	LinkedProductSKU  string `json:"linked_product_sku"`
	LinkedProductType string `json:"linked_product_type"`
	Position          int    `json:"position"`
}

type TierPrices struct {
	TypeOf              string              `json:"type_of"`
	ParentID            int                 `json:"id"`
	CustomerGroupID     int                 `json:"customer_group_id,omitempty"`
	QTY                 int                 `json:"qty,omitempty"`
	Value               int                 `json:"value,omitempty"`
	ExtensionAttributes ExtensionAttributes `json:"extension_attribute"`
}

type ExtensionAttributes struct {
	WebsiteId int `json:"website_id,omitempty"`
}

//SalonPro product struct
type SPProduct struct {
	EntityID             string          `json:"entity_id"`
	Sku                  string          `json:"sku"`
	Name                 string          `json:"name"`
	URL                  string          `json:"url"`
	Price                string          `json:"price"`
	Qty                  int             `json:"qty"`
	IsInStock            interface{}     `json:"is_in_stock"`
	Status               string          `json:"status"`
	CreatedAt            string          `json:"created_at"`
	UpdatedAt            string          `json:"updated_at"`
	Description          string          `json:"description"`
	ShortDescription     string          `json:"short_description"`
	Brand                interface{}     `json:"brand"`
	Supplier             interface{}     `json:"supplier"`
	Color                string          `json:"color"`
	Size                 string          `json:"size"`
	Manufacturer         interface{}     `json:"manufacturer"`
	CountryOfManufacture string          `json:"country_of_manufacture"`
	SpecialPrice         string          `json:"special_price"`
	GroupPrice           string          `json:"group_price"`
	MediaGallery         string          `json:"media_gallery"`
	Weight               string          `json:"weight"`
	MetaTitle            string          `json:"meta_title"`
	MetaKeywords         string          `json:"meta_keywords"`
	MetaDescription      string          `json:"meta_description"`
	TaxClass             string          `json:"tax_class"`
	Categories           []MysqlCategory `json:"categories"`
	TypeID               string          `json:"type_id"`
	Visibility           string          `json:"visibility"`
	OtherAttributes      json.RawMessage `json:"other_attributes"`
}

type MysqlCategory struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

type OtherAttributes struct {
	AttributeSetID      string `json:"attribute_set_id"`
	HasOptions          string `json:"has_options"`
	RequiredOptions     string `json:"required_options"`
	Image               string `json:"image"`
	SmallImage          string `json:"small_image"`
	Thumbnail           string `json:"thumbnail"`
	URLKey              string `json:"url_key"`
	URLPath             string `json:"url_path"`
	OptionsContainer    string `json:"options_container"`
	FoundInRivileAPI    string `json:"found_in_rivile_api"`
	TaxClassID          string `json:"tax_class_id"`
	Date                string `json:"date"`
	SpecialFromDate     string `json:"special_from_date"`
	SpecialToDate       string `json:"special_to_date"`
	NewsFromDate        string `json:"news_from_date"`
	NewsToDate          string `json:"news_to_date"`
	IsSalable           string `json:"is_salable"`
	ImageLabel          string `json:"image_label"`
	SmallImageLabel     string `json:"small_image_label"`
	ThumbnailLabel      string `json:"thumbnail_label"`
	Provider            string `json:"provider,omitempty"`
	SoldToProfessionals string `json:"sold_to_professionals,omitempty"`
	BlakstienuPlotis    string `json:"blakstienu_plotis"`
	BlakstienuIlgis     string `json:"blakstienu_ilgis"`
}

type SalonCategory struct {
	EntityID                   string      `json:"entity_id"`
	ParentID                   string      `json:"parent_id"`
	CreatedAt                  string      `json:"created_at"`
	UpdatedAt                  string      `json:"updated_at"`
	Path                       string      `json:"path"`
	Position                   string      `json:"position"`
	Level                      string      `json:"level"`
	ChildrenCount              string      `json:"children_count"`
	StoreID                    string      `json:"store_id"`
	Addtocart                  interface{} `json:"addtocart"`
	AllChildren                interface{} `json:"all_children"`
	AvailableSortBy            interface{} `json:"available_sort_by"`
	CategoryDescriptionTpl     interface{} `json:"category_description_tpl"`
	CategoryMetaDescriptionTpl interface{} `json:"category_meta_description_tpl"`
	CategoryMetaKeywordsTpl    interface{} `json:"category_meta_keywords_tpl"`
	CategoryMetaTitleTpl       interface{} `json:"category_meta_title_tpl"`
	CategoryTitleTpl           interface{} `json:"category_title_tpl"`
	Children                   interface{} `json:"children"`
	Compareproducts            interface{} `json:"compareproducts"`
	CustomApplyToProducts      string      `json:"custom_apply_to_products"`
	CustomDesign               interface{} `json:"custom_design"`
	CustomDesignFrom           interface{} `json:"custom_design_from"`
	CustomDesignTo             interface{} `json:"custom_design_to"`
	CustomLayoutUpdate         interface{} `json:"custom_layout_update"`
	CustomUseParentSettings    string      `json:"custom_use_parent_settings"`
	DefaultSortBy              interface{} `json:"default_sort_by"`
	Description                interface{} `json:"description"`
	DisplayMode                string      `json:"display_mode"`
	FilterDescriptionTpl       interface{} `json:"filter_description_tpl"`
	FilterMetaDescriptionTpl   interface{} `json:"filter_meta_description_tpl"`
	FilterMetaKeywordsTpl      interface{} `json:"filter_meta_keywords_tpl"`
	FilterMetaTitleTpl         interface{} `json:"filter_meta_title_tpl"`
	FilterPriceRange           interface{} `json:"filter_price_range"`
	FilterTitleTpl             interface{} `json:"filter_title_tpl"`
	Image                      interface{} `json:"image"`
	IncludeInMenu              string      `json:"include_in_menu"`
	IsActive                   string      `json:"is_active"`
	IsAnchor                   string      `json:"is_anchor"`
	LandingPage                interface{} `json:"landing_page"`
	Mailtofriend               interface{} `json:"mailtofriend"`
	MetaDescription            interface{} `json:"meta_description"`
	MetaKeywords               string      `json:"meta_keywords"`
	MetaTitle                  string      `json:"meta_title"`
	Name                       string      `json:"name"`
	PageLayout                 interface{} `json:"page_layout"`
	PathInStore                interface{} `json:"path_in_store"`
	Productname                interface{} `json:"productname"`
	Productprice               interface{} `json:"productprice"`
	ProductDescriptionTpl      interface{} `json:"product_description_tpl"`
	ProductMetaDescriptionTpl  interface{} `json:"product_meta_description_tpl"`
	ProductMetaKeywordsTpl     interface{} `json:"product_meta_keywords_tpl"`
	ProductMetaTitleTpl        interface{} `json:"product_meta_title_tpl"`
	ProductTitleTpl            interface{} `json:"product_title_tpl"`
	Quickview                  interface{} `json:"quickview"`
	Sidebarhider               interface{} `json:"sidebarhider"`
	Thumbnail                  interface{} `json:"thumbnail"`
	URLKey                     string      `json:"url_key"`
	URLPath                    string      `json:"url_path"`
	Wishlist                   interface{} `json:"wishlist"`
	Youtubecode                interface{} `json:"youtubecode"`
}
