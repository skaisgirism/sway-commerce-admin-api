package attribute

import (
	c "bitbucket.org/sway-commerce/sway-commerce-admin-api/config"
	"bitbucket.org/sway-commerce/sway-commerce-admin-api/helpers"
	"encoding/json"
	"fmt"
	"github.com/go-chi/chi"
	"net/http"
)

func createMetadata(w http.ResponseWriter, r *http.Request) {
	var metadata AttributeMetadata
	err := json.NewDecoder(r.Body).Decode(&metadata)
	helpers.PanicErr(err)

	db, err := c.Conf.GetDb()
	helpers.PanicErr(err)

	result, err := db.Exec("INSERT INTO attribute_metadata ("+
		"code, "+
		"type, "+
		"options, "+
		"core, "+
		"format) "+
		" VALUES(?, ?, ?, ?, ?)",
		metadata.Code,
		metadata.Type,
		metadata.Options,
		metadata.Core,
		metadata.Format)
	fmt.Println(result)
	helpers.PanicErr(err)
}

func getMetadata(w http.ResponseWriter, r *http.Request) {
	var metadata AttributeMetadata
	id := chi.URLParam(r, "ID")

	db, err := c.Conf.GetDb()
	helpers.CheckErr(err)

	err = db.QueryRow("SELECT "+
		"id, "+
		"code, "+
		"type, "+
		"options, "+
		"core, "+
		"format "+
		"FROM attribute_metadata m WHERE id=?", id).
		Scan(
			&metadata.ID,
			&metadata.Code,
			&metadata.Type,
			&metadata.Options,
			&metadata.Core,
			&metadata.Format)
	helpers.CheckErr(err)
	err = json.NewEncoder(w).Encode(metadata)
	helpers.PanicErr(err)

}

func getMetadataList(w http.ResponseWriter, r *http.Request) {
	var metadata AttributeMetadata
	var metadatas []AttributeMetadata
	metadatas = []AttributeMetadata{}

	db, err := c.Conf.GetDb()
	helpers.CheckErr(err)

	query, err := db.Query("SELECT id, code, type, options, core, format FROM attribute_metadata")
	helpers.CheckErr(err)
	defer helpers.CloseMySqlRows(query)

	for query.Next() {
		err := query.Scan(
			&metadata.ID,
			&metadata.Code,
			&metadata.Type,
			&metadata.Options,
			&metadata.Core,
			&metadata.Format)
		helpers.CheckErrHttp(err, w)
		metadatas = append(metadatas, metadata)
	}
	err = query.Err()
	helpers.CheckErr(err)
	helpers.SendJsonOk(w, metadatas)
}

func deleteMetadata(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "ID")
	db, err := c.Conf.GetDb()
	helpers.CheckErr(err)

	res, err := db.Exec("DELETE m FROM attribute_metadata m WHERE m.id=?", id)
	fmt.Println(res)
	helpers.CheckErr(err)

}

func updateMetadata(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "ID")
	var metadata AttributeMetadata
	err := json.NewDecoder(r.Body).Decode(&metadata)
	helpers.PanicErr(err)

	db, err := c.Conf.GetDb()
	helpers.PanicErr(err)

	helpers.CheckIfRowExistsInMysql(db, "attribute_metadata", "id", id)

	query, err := db.Prepare("UPDATE attribute_metadata SET " +
		"code=?, " +
		"type=?, " +
		"options=?, " +
		"core=?, " +
		"format=? WHERE id=?")
	helpers.PanicErr(err)

	_, err = query.Exec(metadata.Code,
		metadata.Type,
		metadata.Options,
		metadata.Core,
		metadata.Format,
		id)
	helpers.PanicErr(err)
	if err == nil {
		fmt.Println("Metadata code: " + metadata.Code + " updated in mysql")
	}
}
