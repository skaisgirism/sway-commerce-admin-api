package attribute

import (
	"github.com/go-chi/chi"
	"net/http"
)

func RouterAttribute() http.Handler {
	r := chi.NewRouter()
	//Product attribute metadata routing
	r.Post("/metadata/create", createMetadata)
	r.Get("/metadata/{ID}", getMetadata)
	r.Get("/metadata/", getMetadataList)
	r.Delete("/metadata/{ID}", deleteMetadata)
	r.Put("/metadata/{ID}", updateMetadata)

	//Product attribute set routing
	r.Post("/set/create", createSet)
	r.Get("/set/{ID}", getSet)
	r.Get("/set/", getSetList)
	r.Delete("/set/{ID}", deleteSet)
	r.Put("/set/{ID}", updateSet)
	return r
}
