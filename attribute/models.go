package attribute

import "encoding/json"

type AttributeMetadata struct {
	ID      int              `json:"id"`
	Code    string           `json:"code"`
	Type    string           `json:"type"`
	Options *json.RawMessage `json:"options,omitempty"`
	Core    bool             `json:"core"`
	Format  *string          `json:"format,omitempty"`
}

type AttributeSet struct {
	ID            int             `json:"id"`
	Name          string          `json:"name"`
	AttributeList json.RawMessage `json:"attribute_list,omitempty"`
}
