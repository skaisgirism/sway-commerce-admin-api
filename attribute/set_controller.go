package attribute

import (
	c "bitbucket.org/sway-commerce/sway-commerce-admin-api/config"
	"bitbucket.org/sway-commerce/sway-commerce-admin-api/helpers"
	"encoding/json"
	"fmt"
	"github.com/go-chi/chi"
	"net/http"
)

func createSet(w http.ResponseWriter, r *http.Request) {
	var set AttributeSet
	err := json.NewDecoder(r.Body).Decode(&set)
	helpers.CheckErr(err)

	fmt.Println(set)

	db, err := c.Conf.GetDb()
	helpers.PanicErr(err)

	result, err := db.Exec("INSERT INTO attribute_set ("+
		"name, "+
		"attribute_list) "+
		" VALUES(?, ?)",
		set.Name,
		set.AttributeList)
	fmt.Println(result)
	helpers.PanicErr(err)
}

func getSet(w http.ResponseWriter, r *http.Request) {
	var set AttributeSet
	id := chi.URLParam(r, "ID")

	db, err := c.Conf.GetDb()
	helpers.CheckErr(err)

	err = db.QueryRow("SELECT "+
		"id, "+
		"name, "+
		"attribute_list FROM attribute_set s WHERE id=?", id).
		Scan(
			&set.ID,
			&set.Name,
			&set.AttributeList)
	helpers.CheckErr(err)
	err = json.NewEncoder(w).Encode(set)
	helpers.PanicErr(err)

}

func getSetList(w http.ResponseWriter, r *http.Request) {
	var set AttributeSet
	var sets []AttributeSet
	sets = []AttributeSet{}

	db, err := c.Conf.GetDb()
	helpers.CheckErr(err)

	query, err := db.Query("SELECT id, name, attribute_list FROM attribute_set")
	helpers.CheckErr(err)
	defer helpers.CloseMySqlRows(query)

	for query.Next() {
		err := query.Scan(
			&set.ID,
			&set.Name,
			&set.AttributeList)
		helpers.CheckErr(err)
		sets = append(sets, set)
	}
	err = query.Err()
	helpers.CheckErr(err)
	helpers.SendJsonOk(w, sets)
	//w.Header().Set("Content-Type", "application/json")
	//err = json.NewEncoder(w).Encode(sets)
	//helpers.PanicErr(err)
}

func deleteSet(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "ID")
	db, err := c.Conf.GetDb()
	helpers.CheckErr(err)

	res, err := db.Exec("DELETE s FROM attribute_set s WHERE s.id=?", id)
	fmt.Println(res)
	helpers.CheckErr(err)

}

func updateSet(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "ID")
	var set AttributeSet
	err := json.NewDecoder(r.Body).Decode(&set)
	helpers.PanicErr(err)

	db, err := c.Conf.GetDb()
	helpers.PanicErr(err)

	helpers.CheckIfRowExistsInMysql(db, "attribute_set", "id", id)

	query, err := db.Prepare("UPDATE attribute_set SET " +
		"name=?, " +
		"attribute_list=? WHERE id=?")
	helpers.PanicErr(err)

	_, err = query.Exec(set.Name,
		set.AttributeList,
		id)
	helpers.PanicErr(err)
	if err == nil {
		fmt.Println("Attribute set: " + set.Name + " updated in mysql")
	}
}
