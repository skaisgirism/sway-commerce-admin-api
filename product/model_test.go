package product

import (
	"database/sql"
	"reflect"
	"testing"
)

var p1002 = Product{EntityID:"1002", Sku: "sku1002",
Name:"Product 1002", Description:"Description 1002",
ShortDescription: "Short description 1002",
IsInStock: 1}
var p1003 = Product{EntityID:"1003", Sku: "sku1003",
Name:"Product 1003", Description:"Description 1003",
ShortDescription: "Short description 1003",
IsInStock: 1}

func Test_insertProduct(t *testing.T) {
	initConf(t)
	product, err := insertProduct(p1003)
	if err != nil {
		t.Error("Error on insertProduct. ", err)
	}

	if product == nil {
		t.Error("returned product is nil")
	}

	if product.EntityID == "" {
		t.Error("returned product has entityID = ''")
	}
}

func TestUpdateProduct(t *testing.T) {
	initConf(t)
	mt := "meta title 1002 updated"
	p := Product{EntityID:"1002", Sku: "sku1002update",
		Name:"Product 1002 updated", Description:"Description 1002 updated",
		ShortDescription: "Short description 1002 updated", Brand:"brand 1002 updated",
		MetaTitle: &mt,
		IsInStock: 2}
	result, err := UpdateProduct(p)
	if err != nil {
		t.Error(err)
	}
	if result == nil {
		t.Error("result is nil")
	}
}

func TestDeleteProduct(t *testing.T) {
	initConf(t)
	productId := "1003"
	deleted, err := DeleteProduct(productId)
	if err != nil {
		t.Error(err)
	}

	if deleted == false {
		t.Errorf("unable to delete product with id: %s", productId)
	}
}

func TestReadProduct(t *testing.T) {
	type args struct {
		productId string
	}
	tests := []struct {
		name    string
		args    args
		want    *Product
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ReadProduct(tt.args.productId)
			if (err != nil) != tt.wantErr {
				t.Errorf("ReadProduct() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ReadProduct() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestReadProducts(t *testing.T) {
	tests := []struct {
		name    string
		want    []Product
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ReadProducts()
			if (err != nil) != tt.wantErr {
				t.Errorf("ReadProducts() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ReadProducts() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUpdateProduct1(t *testing.T) {
	type args struct {
		p Product
	}
	tests := []struct {
		name    string
		args    args
		want    sql.Result
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := UpdateProduct(tt.args.p)
			if (err != nil) != tt.wantErr {
				t.Errorf("UpdateProduct() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UpdateProduct() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_insertProduct1(t *testing.T) {
	type args struct {
		product Product
	}
	tests := []struct {
		name    string
		args    args
		want    *Product
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := insertProduct(tt.args.product)
			if (err != nil) != tt.wantErr {
				t.Errorf("insertProduct() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("insertProduct() got = %v, want %v", got, tt.want)
			}
		})
	}
}