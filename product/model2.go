package product

import "encoding/json"

type FinalProduct struct {
	EntityID             string          `json:"entity_id"`
	Sku                  string          `json:"sku"`
	Name                 string          `json:"name"`
	URL                  string          `json:"url"`
	Price                string          `json:"price"`
	Qty                  int64           `json:"qty"`
	IsInStock            string          `json:"is_in_stock"`
	Status               string          `json:"status"`
	CreatedAt            string          `json:"created_at"`
	UpdatedAt            string          `json:"updated_at"`
	Description          string          `json:"description"`
	ShortDescription     string          `json:"short_description"`
	Brand                string          `json:"brand"`
	Supplier             string          `json:"supplier"`
	Color                string          `json:"color"`
	Size                 string          `json:"size"`
	Manufacturer         string          `json:"manufacturer"`
	CountryOfManufacture string          `json:"country_of_manufacture"`
	SpecialPrice         string          `json:"special_price"`
	GroupPrice           string          `json:"group_price"`
	MediaGallery         string          `json:"media_gallery"`
	Weight               string          `json:"weight"`
	MetaTitle            string          `json:"meta_title"`
	MetaKeywords         string          `json:"meta_keywords"`
	MetaDescription      string          `json:"meta_description"`
	TaxClass             string          `json:"tax_class"`
	TypeID               string          `json:"type_id"`
	Visibility           string          `json:"visibility"`
	OtherAttributes      json.RawMessage `json:"other_attributes"`
}

type FinalOtherAttributes struct {
	AttributeSetID      string `json:"attribute_set_id"`
	HasOptions          string `json:"has_options"`
	RequiredOptions     string `json:"required_options"`
	Image               string `json:"image"`
	SmallImage          string `json:"small_image"`
	Thumbnail           string `json:"thumbnail"`
	URLKey              string `json:"url_key"`
	URLPath             string `json:"url_path"`
	OptionsContainer    string `json:"options_container"`
	FoundInRivileAPI    string `json:"found_in_rivile_api"`
	TaxClassID          string `json:"tax_class_id"`
	Date                string `json:"date"`
	SpecialFromDate     string `json:"special_from_date"`
	SpecialToDate       string `json:"special_to_date"`
	NewsFromDate        string `json:"news_from_date"`
	NewsToDate          string `json:"news_to_date"`
	IsSalable           string `json:"is_salable"`
	ImageLabel          string `json:"image_label"`
	SmallImageLabel     string `json:"small_image_label"`
	ThumbnailLabel      string `json:"thumbnail_label"`
	Provider            string `json:"provider,omitempty"`
	SoldToProfessionals string `json:"sold_to_professionals,omitempty"`
	BlakstienuPlotis    string `json:"blakstienu_plotis"`
	BlakstienuIlgis     string `json:"blakstienu_ilgis"`
}
