package product

import (
	c "bitbucket.org/sway-commerce/sway-commerce-admin-api/config"
	"bitbucket.org/sway-commerce/sway-commerce-admin-api/helpers"
	"database/sql"
	"encoding/json"
	"errors"
	log "github.com/sirupsen/logrus"
	"net/http"
	"strings"
)

type Product struct {
	EntityID             string          `json:"entity_id" db:"entity_id"`
	Sku                  string          `json:"sku" db:"sku"`
	Name                 string          `json:"name" db:"name"`
	URL                  string          `json:"url"`
	Price                string          `json:"price"`
	Qty                  int64           `json:"qty"`
	IsInStock            interface{}            `json:"is_in_stock" db:"is_in_stock"`
	Status               string          `json:"status"`
	CreatedAt            string          `json:"created_at" db:"created_at"`
	UpdatedAt            string          `json:"updated_at" db:"updated_at"`
	Description          string          `json:"description"`
	ShortDescription     interface{}     `json:"short_description" db:"short_description"`
	Brand                interface{}     `json:"brand"`
	Supplier             interface{}     `json:"supplier"`
	Color                interface{}     `json:"color"`
	Size                 interface{}     `json:"size"`
	Manufacturer         interface{}     `json:"manufacturer"`
	CountryOfManufacture interface{}     `json:"country_of_manufacture" db:"country_of_manufacture"`
	SpecialPrice         *string         `json:"special_price" db:"special_price"`
	GroupPrice           string          `json:"group_price" db:"group_price"`
	MediaGallery         string          `json:"media_gallery" db:"media_gallery"`
	Weight               interface{}     `json:"weight"`
	MetaTitle            *string         `json:"meta_title" db:"meta_title"`
	MetaKeywords         *string         `json:"meta_keywords" db:"meta_keywords"`
	MetaDescription      *string         `json:"meta_description" db:"meta_description"`
	TaxClass             string          `json:"tax_class" db:"tax_class"`
	Categories           []Categories    `json:"categories" db:"categories"`
	CategoryID           string          `json:"category_id" db:"category_id"`
	TypeID               string          `json:"type_id" db:"type_id"`
	Visibility           string          `json:"visibility"`
	OtherAttributes      json.RawMessage `json:"other_attributes" db:"other_attributes"`
}

type Categories struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

type OtherAttributes struct {
	EntityTypeID               string        `json:"entity_type_id"`
	AttributeSetID             string        `json:"attribute_set_id"`
	HasOptions                 string        `json:"has_options"`
	RequiredOptions            string        `json:"required_options"`
	Cost                       interface{}   `json:"cost"`
	Msrp                       interface{}   `json:"msrp"`
	Image                      string        `json:"image"`
	SmallImage                 string        `json:"small_image"`
	Thumbnail                  string        `json:"thumbnail"`
	URLKey                     string        `json:"url_key"`
	URLPath                    string        `json:"url_path"`
	CustomDesign               interface{}   `json:"custom_design"`
	PageLayout                 interface{}   `json:"page_layout"`
	OptionsContainer           string        `json:"options_container"`
	MsrpEnabled                string        `json:"msrp_enabled"`
	MsrpDisplayActualPriceType string        `json:"msrp_display_actual_price_type"`
	GiftMessageAvailable       interface{}   `json:"gift_message_available"`
	FoundInRivileAPI           *string       `json:"found_in_rivile_api"`
	IsRecurring                *string       `json:"is_recurring,omitempty"`
	EnableGooglecheckout       string        `json:"enable_googlecheckout"`
	TaxClassID                 string        `json:"tax_class_id"`
	IsImported                 string        `json:"is_imported"`
	Featured                   string        `json:"featured"`
	MageFeaturedProduct        string        `json:"mage_featured_product"`
	Date                       *string       `json:"date"`
	ProductWeight              interface{}   `json:"product_weight"`
	MetaKeyword                *string       `json:"meta_keyword"`
	CustomLayoutUpdate         interface{}   `json:"custom_layout_update"`
	SpecialFromDate            *string       `json:"special_from_date"`
	SpecialToDate              interface{}   `json:"special_to_date"`
	NewsFromDate               *string       `json:"news_from_date"`
	NewsToDate                 *string       `json:"news_to_date"`
	CustomDesignFrom           interface{}   `json:"custom_design_from"`
	CustomDesignTo             interface{}   `json:"custom_design_to"`
	GroupPriceChanged          int64         `json:"group_price_changed"`
	TierPrice                  []interface{} `json:"tier_price"`
	TierPriceChanged           int64         `json:"tier_price_changed"`
	StockItem                  struct{}      `json:"stock_item"`
	IsSalable                  interface{}   `json:"is_salable"`
	RequestPath                bool          `json:"request_path"`
	ImageLabel                 *string       `json:"image_label"`
	SmallImageLabel            *string       `json:"small_image_label"`
	ThumbnailLabel             *string       `json:"thumbnail_label"`
	SmdCustomStockStatus       interface{}   `json:"smd_custom_stock_status"`
	UseSmdColorswatch          *string       `json:"use_smd_colorswatch,omitempty"`
	SmdColorswatchProductList  *string       `json:"smd_colorswatch_product_list,omitempty"`
	WomenLike                  *string       `json:"women_like,omitempty"`
	Wights                     interface{}   `json:"wights"`
	Provider                   *string       `json:"provider,omitempty"`
	SoldToProfessionals        *string       `json:"sold_to_professionals,omitempty"`
	BlakstienuPlotis           interface{}   `json:"blakstienu_plotis"`
	BlakstienuIlgis            interface{}   `json:"blakstienu_ilgis"`
	Summary                    *string       `json:"summary"`
	ManufacturerInfo           *string       `json:"manufacturer_info"`
}

var productFields = []string{"entity_id", "sku", "name", "url", "price", "qty", "is_in_stock", "status",
	"created_at", "updated_at", "description", "brand", "supplier", "color", "size",
	"manufacturer", "country_of_manufacture", "special_price", "group_price",
	"media_gallery", "weight", "meta_title", "meta_keywords", "meta_description",
	"tax_class", "category_id", "type_id", "visibility", "other_attributes"}

var productFieldsString = strings.Join(productFields, ",")

var ErrNoData = errors.New("product: product not found")

func ReadProduct(productId string) (*Product, error) {

	db, err := c.Conf.GetDb()
	if err != nil {
		return nil, err
	}

	var product Product
	err = db.Get(&product, db.Rebind("SELECT "+productFieldsString+" FROM products p WHERE entity_id=?"), productId)
	if err == sql.ErrNoRows {
		return nil, ErrNoData
	}
	return &product, err
}

func ReadProducts() ([]Product, error) {
	db, err := c.Conf.GetDb()
	if err != nil {
		return nil, err
	}
	var products []Product
	err = db.Select(&products, "SELECT "+productFieldsString+" FROM products;")
	return products, nil
}

func insertProduct(product Product) (*Product, error) {
	db, err := c.Conf.GetDb()
	helpers.PanicErr(err)

	q3 := `INSERT INTO products (` + productFieldsString + `) VALUES (
	:entity_id, :sku, :name, :url, :price, :qty, :is_in_stock, :status, :created_at, :updated_at, :description,
	:brand, :supplier, :color, :size, :manufacturer, :country_of_manufacture, :special_price, :group_price,
	:media_gallery, :weight, :meta_title, :meta_keywords, :meta_description, :tax_class, :category_id, :type_id,
	:visibility, :other_attributes)`
	result, err := db.NamedExec(q3, product)
	if err != nil {
		log.Fatal(err)
	}

	affected, err := result.RowsAffected()
	if err != nil {
		return nil, err
	}

	if affected > 0 {
		lastId, err := result.LastInsertId()
		if err != nil {
			return nil, err
		}
		log.Infof("insertProduct lastInsertId: %d", lastId)
	}

	return &product, nil
}

func UpdateProduct(p Product) (sql.Result, error) {
	db, err := c.Conf.GetDb()
	helpers.PanicErr(err)

	updateFields := getUpdateString(productFields)
	//query, err := db.Prepare("UPDATE products SET " + updateFields + " ? WHERE entity_id=?")
	result, err := db.NamedExec(`UPDATE products SET `+updateFields+` WHERE entity_id="`+p.EntityID+`"`, p)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func DeleteProduct(productId string) (bool, error) {
	db, err := c.Conf.GetDb()
	helpers.PanicErr(err)

	stmt, err := db.Prepare(`DELETE FROM products WHERE entity_id=?`)
	if err != nil {
		return false, err
	}
	result, err := stmt.Exec(productId)
	if err != nil {
		return false, err
	}
	affected, err := result.RowsAffected()
	if err != nil {
		return false, err
	}
	return affected > 0, nil
}

//Decodes body from URL and returns a SalonPro product array
func LoadProductDataFromUrl(url string) ([]Product, error) {
	//url := "https://www.salonpro.lt/productsjson?page=1"
	resp, err := http.Get(url)
	helpers.CheckErr(err)
	defer resp.Body.Close()
	productList := make([]Product, 0)
	err = json.NewDecoder(resp.Body).Decode(&productList)
	if err != nil && err.Error() != "EOF" {
		return nil, err
	}
	return productList, nil
}
