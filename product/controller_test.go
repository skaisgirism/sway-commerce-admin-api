package product

import (
	"bitbucket.org/sway-commerce/sway-commerce-admin-api/config"
	"testing"
)

func initConf(t *testing.T) {
	// init database
	conf := config.GetConfig("../test.yml")
	if conf == nil {
		t.Error("conf is nil")
	}
}

func Test_ReadProduct(t *testing.T) {
	initConf(t)
	product, err := ReadProduct("16447")
	if err != nil {
		t.Error(err)
	}
	if product == nil {
		t.Error("nil product returned")
	}
}

func Test_ReadProducts(t *testing.T) {
	initConf(t)
	products, err := ReadProducts()
	if err != nil {
		t.Error(err)
	}

	if len(products) == 0 {
		t.Error("returned empty product list")
	}
}

// export data
// database 'go-api-ws'
// table products
// table product
// table product_admin
// mysqldump -t -u MyUserName -pMyPassword MyDatabase MyTable --where="ID = 10"
// mysqldump -t -u RemoteAdmin -pSwayAdmin go-api-ws products --where="entity_id = 16447"