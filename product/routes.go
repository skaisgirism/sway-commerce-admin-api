package product

import (
	"github.com/go-chi/chi"
	"net/http"
)

func RouterProduct() http.Handler {
	r := chi.NewRouter()
	r.Post("/create", createProduct)
	r.Post("/createmultiple", createMultipleFromUrl)
	r.Get("/{EntityID}", getProduct)
	r.Get("/", getProductList)
	r.Delete("/{EntityID}", deleteProduct)
	r.Put("/{EntityID}", updateProduct)
	return r
}
