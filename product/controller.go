package product

import (
	c "bitbucket.org/sway-commerce/sway-commerce-admin-api/config"
	"bitbucket.org/sway-commerce/sway-commerce-admin-api/helpers"
	"encoding/json"
	"fmt"
	"github.com/go-chi/chi"
	"net/http"
)

func getUpdateString(fields []string) string {
	const separator = ","
	result := fields[0] + "=:" + fields[0]
	for _, s := range fields[1:] {
		result = result + separator + s + "=:" + s
	}
	return result
}

//Adding a single product to MySQL db
func createProduct(w http.ResponseWriter, r *http.Request) {
	var product Product
	err := json.NewDecoder(r.Body).Decode(&product)
	helpers.PanicErr(err)

	if product.Manufacturer == false {
		product.Manufacturer = nil
	}

	if product.Brand == false {
		product.Brand = nil
	}

	//Is in stock remap functionality
	if product.IsInStock == "1" || true {
		product.IsInStock = 1
	}else{
		product.IsInStock = 0
	}

	if product.Supplier == false {
		product.Supplier = nil
	}

	for i := range product.Categories {
		if product.CategoryID == "" {
			product.CategoryID = product.Categories[i].ID
		} else {
			product.CategoryID = product.CategoryID + ", " + product.Categories[i].ID
		}
	}

	result, err := insertProduct(product)

	fmt.Println(result)
	helpers.CheckErrHttp(err, w)
}

func createMultipleFromUrl(w http.ResponseWriter, r *http.Request) {
	salonProductList, err := LoadProductDataFromUrl("https://www.salonpro.lt/productsjson?page=1")
	helpers.CheckErr(err)

	for i := range salonProductList {
		result, err := insertProduct(salonProductList[i])
		fmt.Println(result)
		helpers.CheckErrHttp(err, w)
	}
}

func deleteProduct(w http.ResponseWriter, r *http.Request) {
	entityID := chi.URLParam(r, "EntityID")
	db, err := c.Conf.GetDb()
	helpers.CheckErr(err)

	res, err := db.Exec("DELETE p FROM products p WHERE p.entity_id=?", entityID)
	fmt.Println(res)
	helpers.CheckErrHttp(err, w)
}

func getProduct(w http.ResponseWriter, r *http.Request) {
	entityID := chi.URLParam(r, "EntityID")
	product, err := ReadProduct(entityID)
	if err == ErrNoData {
		helpers.SendJSON(w, http.StatusNotFound, nil)
	} else if err != nil {
		helpers.SendJsonError(err, w)
	} else {
		helpers.SendJsonOk(w, product)
	}
}

func getProductList(w http.ResponseWriter, r *http.Request) {
	products, err := ReadProducts()
	if err != nil {
		helpers.SendJsonError(err, w)
		return
	}
	helpers.SendJsonOk(w, products)
}

func updateProduct(w http.ResponseWriter, r *http.Request) {
	entityID := chi.URLParam(r, "EntityID")
	var product Product
	err := json.NewDecoder(r.Body).Decode(&product)
	if product.EntityID != entityID {
		helpers.SendJSON(w, http.StatusBadRequest, "entityID in URI does not match entityID in payload product.")
		return
	}
	helpers.PanicErr(err)
	result, err := UpdateProduct(product)
	helpers.CheckErrHttp(err, w)
	helpers.SendJsonOk(w, result)
}
