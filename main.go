package main

import (
	"bitbucket.org/sway-commerce/sway-commerce-admin-api/attribute"
	"bitbucket.org/sway-commerce/sway-commerce-admin-api/category"
	"bitbucket.org/sway-commerce/sway-commerce-admin-api/config"
	"bitbucket.org/sway-commerce/sway-commerce-admin-api/health"
	"bitbucket.org/sway-commerce/sway-commerce-admin-api/helpers"
	"bitbucket.org/sway-commerce/sway-commerce-admin-api/product"
	"bitbucket.org/sway-commerce/sway-commerce-admin-api/solr/solrCategory"
	"bitbucket.org/sway-commerce/sway-commerce-admin-api/solr/solrProduct"
	"github.com/go-chi/chi"
	"github.com/go-chi/cors"
	log "github.com/sirupsen/logrus"
	"net/http"
)

func init() {
	config.GetConfig("config.yml")
	helpers.InitLogRus()
}

func main() {
	log.Println("Starting service")
	r := chi.NewRouter()
	// Basic CORS
	// for more ideas, see: https://developer.github.com/v3/#cross-origin-resource-sharing
	cors := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: true,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	})
	r.Use(cors.Handler)
	r.Mount("/product", product.RouterProduct())
	r.Mount("/health", health.RouterHealth())
	r.Mount("/category", category.RouterCategory())
	r.Mount("/attribute", attribute.RouterAttribute())
	r.Mount("/solr", solrProduct.RouterSolrProduct())
	r.Mount("/solrtest", solrCategory.RouterSolrCategory())
	//r.Mount("/attribute", attribute.RouterAttributeSet())

	log.Println("---------------------------------------------------------------------------")
	log.Println("--")
	log.Println("--  Started sway-commerce-admin-api on " + config.Conf.Port)
	log.Println("--")
	log.Println("---------------------------------------------------------------------------")
	err := http.ListenAndServe(config.Conf.Port, r)
	helpers.PanicErr(err)
}
