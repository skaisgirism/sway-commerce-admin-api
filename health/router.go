package health

import (
	"github.com/go-chi/chi"
	"net/http"
)

func RouterHealth() http.Handler {
	r := chi.NewRouter()
	r.Get("/", healthCheck)
	return r
}
