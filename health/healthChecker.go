package health

import (
	"bitbucket.org/sway-commerce/sway-commerce-admin-api/helpers"
	"net/http"
)

func healthCheck(w http.ResponseWriter, r *http.Request) {
	helpers.SendJsonOk(w, "ok")
}
