package solrTest2

import (
	"bitbucket.org/sway-commerce/sway-commerce-admin-api/helpers"
	"bitbucket.org/sway-commerce/sway-commerce-admin-api/solrTest2/vueMapper"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

//Decodes body from URL and returns a SalonPro product array
func LoadProductDataFromUrl(url string) ([]vueMapper.SPProduct, error) {
	//url := "https://www.salonpro.lt/productsjson?page=1"
	resp, err := http.Get(url)
	helpers.CheckErr(err)
	defer resp.Body.Close()
	productList := make([]vueMapper.SPProduct, 0)
	err = json.NewDecoder(resp.Body).Decode(&productList)
	if err != nil && err.Error() != "EOF" {
		return nil, err
	}
	return productList, nil
}

//Uploads product data from json file to solrTest2
func LoadProductDataFromJson(fileName string) ([]vueMapper.VueProduct, error) {
	var productList []vueMapper.VueProduct
	infoFile, err := os.Open(fileName)
	if err != nil {
		//return productList, err
		log.Fatal(err)
	}
	defer infoFile.Close()
	fmt.Println(fileName, " Opened")

	byteValue, _ := ioutil.ReadAll(infoFile)
	json.Unmarshal(byteValue, &productList)

	//Send the productList to solrTest2
	DataPusher(productList)

	return productList, err
}

//Pushes the data to solrTest2
func DataPusher(data interface{}) error {
	// Makes copy of struct to buffer
	structCopy := new(bytes.Buffer)
	//Encode it
	json.NewEncoder(structCopy).Encode(data)

	resp, err := http.Post("http://192.168.0.2:8983/solrTest2/salonProCore/update/json/docs?&commit=true", helpers.MIMEApplicationJSON, structCopy)
	helpers.CheckErr(err)

	//  response log
	b, _ := ioutil.ReadAll(resp.Body)
	fmt.Printf("%s", b)

	return err
}

func PushCategories(data interface{}) error {
	//Makes copy of struct to Buffer
	structCopy := new(bytes.Buffer)
	//Encodes it
	json.NewEncoder(structCopy).Encode(data)
	//Sends encoded structCopy to Solr search engine
	//Post method url contains all the parameters how Solar should handle the body
	resp, err := http.Post("http://192.168.0.2:8983/solrTest2/storefrontCore/update/json/docs?"+
		"split=/_source"+
		"|/_source/_childDocuments_"+
		"&f=/**"+
		"&commit=true",
		helpers.MIMEApplicationJSON, structCopy)
	//Catch error
	helpers.CheckErr(err)
	//log our response
	b, _ := ioutil.ReadAll(resp.Body)
	fmt.Printf("%s", b)

	return err
}

// Function that takes SolrDataStrut as parameter,
// splits it to pieces defined by size parameter,
// and calls PushCategories() functions for each piece
func SplitCatalog(catalog vueMapper.SolrDataStruct, size int) {
	//Simple product
	if len(catalog.SimpleProductStruct) > size {
		var slicesAmount = 1 + len(catalog.SimpleProductStruct)/size
		var startFrom = 0
		catalogSlices := make([][]vueMapper.SimpleProductStruct, slicesAmount)
		for i := 1; i < slicesAmount+1; i++ {
			if size*i > len(catalog.SimpleProductStruct) {
				var length = len(catalog.SimpleProductStruct)
				catalogSlices[i-1] = append(catalog.SimpleProductStruct[startFrom:length])
			} else {
				catalogSlices[i-1] = append(catalog.SimpleProductStruct[startFrom : size*i])
				startFrom = startFrom + size
			}
			PushCategories(catalogSlices[i-1])
		}
	} else {
		PushCategories(catalog.SimpleProductStruct)
	}
	//
	////Configurable product
	//if len(catalog.ConfigurableProductStruct) > size {
	//	var slicesAmount = 1 + len(catalog.ConfigurableProductStruct)/size
	//	var startFrom = 0
	//	catalogSlices := make([][]vueMapper.ConfigurableProductStruct, slicesAmount)
	//	for i := 1; i < slicesAmount+1; i++ {
	//		if size*i > len(catalog.ConfigurableProductStruct) {
	//			var length = len(catalog.ConfigurableProductStruct)
	//			catalogSlices[i-1] = append(catalog.ConfigurableProductStruct[startFrom:length])
	//		} else {
	//			catalogSlices[i-1] = append(catalog.ConfigurableProductStruct[startFrom : size*i])
	//			startFrom = startFrom + size
	//		}
	//		//for g := range catalogSlices {
	//		//	for h := range catalogSlices[g] {
	//		//		fmt.Println(catalogSlices[g][h].Source.ChildDocuments)
	//		//	}
	//		//}
	//		PushCategories(catalogSlices[i-1])
	//	}
	//} else {
	//	PushCategories(catalog.ConfigurableProductStruct)
	//}
	//
	////Category
	//if len(catalog.CategoryStruct) > size {
	//	var slicesAmount = 1 + len(catalog.CategoryStruct)/size
	//	var startFrom = 0
	//	catalogSlices := make([][]vueMapper.CategoryStruct, slicesAmount)
	//	for i := 1; i < slicesAmount+1; i++ {
	//		if size*i > len(catalog.CategoryStruct) {
	//			var length = len(catalog.CategoryStruct)
	//			catalogSlices[i-1] = append(catalog.CategoryStruct[startFrom:length])
	//		} else {
	//			catalogSlices[i-1] = append(catalog.CategoryStruct[startFrom : size*i])
	//			startFrom = startFrom + size
	//		}
	//		PushCategories(catalogSlices[i-1])
	//	}
	//} else {
	//	PushCategories(catalog.CategoryStruct)
	//}
	//
	////Attribute
	//if len(catalog.AttributeStruct) > size {
	//	var slicesAmount = 1 + len(catalog.AttributeStruct)/size
	//	var startFrom = 0
	//	catalogSlices := make([][]vueMapper.AttributeStruct, slicesAmount)
	//	for i := 1; i < slicesAmount+1; i++ {
	//		if size*i > len(catalog.AttributeStruct) {
	//			var length = len(catalog.AttributeStruct)
	//			catalogSlices[i-1] = append(catalog.AttributeStruct[startFrom:length])
	//		} else {
	//			catalogSlices[i-1] = append(catalog.AttributeStruct[startFrom : size*i])
	//			startFrom = startFrom + size
	//		}
	//		PushCategories(catalogSlices[i-1])
	//	}
	//} else {
	//	PushCategories(catalog.AttributeStruct)
	//}
	//
	////TaxRule
	//if len(catalog.TaxRuleStruct) > size {
	//	var slicesAmount = 1 + len(catalog.TaxRuleStruct)/size
	//	var startFrom = 0
	//	catalogSlices := make([][]vueMapper.TaxRuleStruct, slicesAmount)
	//	for i := 1; i < slicesAmount+1; i++ {
	//		if size*i > len(catalog.TaxRuleStruct) {
	//			var length = len(catalog.TaxRuleStruct)
	//			catalogSlices[i-1] = append(catalog.TaxRuleStruct[startFrom:length])
	//		} else {
	//			catalogSlices[i-1] = append(catalog.TaxRuleStruct[startFrom : size*i])
	//			startFrom = startFrom + size
	//		}
	//		PushCategories(catalogSlices[i-1])
	//	}
	//} else {
	//	PushCategories(catalog.TaxRuleStruct)
	//}
}
