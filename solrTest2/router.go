package solrTest2

import (
	"github.com/go-chi/chi"
	"net/http"
)

func RouterSolr() http.Handler {
	r := chi.NewRouter()
	r.Post("/", productDataPushToSolr)
	return r
}
