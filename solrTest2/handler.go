package solrTest2

import (
	"bitbucket.org/sway-commerce/sway-commerce-admin-api/helpers"
	"bitbucket.org/sway-commerce/sway-commerce-admin-api/solrTest2/vueMapper"
	"encoding/json"
	"net/http"
)

func productDataPushToSolr(w http.ResponseWriter, r *http.Request) {
	salonProductList, err := LoadProductDataFromUrl("https://www.salonpro.lt/productsjson?page=1")
	helpers.CheckErr(err)

	vueProductList, err := vueMapper.VueProductListMapper(salonProductList)
	helpers.CheckErr(err)

	var vueStruct vueMapper.SolrDataStruct

	vueStruct.SimpleProductStruct = vueProductList

	vueStruct = modifyDataAmount(vueStruct, 500)
	SplitCatalog(vueStruct, 100)

	//err = PushCategories(vueStruct.SimpleProductStruct)
	//helpers.CheckErr(err)

	if err == nil {
		json.NewEncoder(w).Encode("ok")
	} else {
		json.NewEncoder(w).Encode(err)
	}
}
