package vueMapper

import (
	"bitbucket.org/sway-commerce/sway-commerce-admin-api/helpers"
	"encoding/json"
	"fmt"
	"os"
)

//func VueProductListMapper(productList []SPProduct) ([]VueProduct, error){
//	var vueProduct = DefaultVueProduct
//	var vueProductList []VueProduct
//	var err error
//
//	var otherAttributes OtherAttributes
//
//	for index := range productList {
//		salonProduct := productList[index]
//		err = json.Unmarshal(salonProduct.OtherAttributes, &otherAttributes)
//		helpers.CheckErr(err)
//
//		if(salonProduct.Manufacturer == false){
//			salonProduct.Manufacturer = nil
//		}
//
//		if(salonProduct.Brand == false){
//			salonProduct.Brand = nil
//		}
//
//		if(salonProduct.IsInStock == 0){
//			salonProduct.IsInStock = false
//		}
//		if(salonProduct.IsInStock == 1){
//			salonProduct.IsInStock = true
//		}
//
//		if(salonProduct.Supplier == false){
//			salonProduct.Supplier = nil
//		}
//
//		if salonProduct.TypeID == "virtual" {
//			fmt.Println("product sku: "+ salonProduct.Sku + " was virtual")
//		}else{
//		vueProduct.ID = extractInt(salonProduct.EntityID)
//		vueProduct.Sku = salonProduct.Sku
//		vueProduct.Name = salonProduct.Name
//		vueProduct.AttributeSetID = extractInt(otherAttributes.AttributeSetID)
//		vueProduct.Price = extractPrice(salonProduct.Price)
//		vueProduct.Status = extractInt(salonProduct.Status)
//		vueProduct.Visibility = extractInt(salonProduct.Visibility)
//		vueProduct.TypeID = salonProduct.TypeID
//		vueProduct.CreatedAt = salonProduct.CreatedAt
//		vueProduct.UpdatedAt = salonProduct.UpdatedAt
//		vueProduct.FinalPrice = extractPrice(salonProduct.Price)
//		vueProduct.MaxPrice = extractPrice(salonProduct.Price)
//		vueProduct.MaxRegularPrice = extractPrice(salonProduct.Price)
//		vueProduct.MinimalRegularPrice = extractPrice(salonProduct.Price)
//		vueProduct.MinimalPrice = extractPrice(salonProduct.Price)
//		vueProduct.RegularPrice = extractPrice(salonProduct.Price)
//		vueProduct.ItemID = extractInt(salonProduct.EntityID)
//		vueProduct.ProductID = extractInt(salonProduct.EntityID)
//		vueProduct.Qty = salonProduct.Qty
//		vueProduct.IsInStock = extractBool(salonProduct.IsInStock)
//		vueProduct.Description = salonProduct.Description
//		vueProduct.Image = otherAttributes.Image
//		vueProduct.SmallImage = otherAttributes.SmallImage
//		vueProduct.Thumbnail = otherAttributes.Thumbnail
//		vueProduct.OptionsContainer = otherAttributes.OptionsContainer
//		vueProduct.RequiredOptions = otherAttributes.RequiredOptions
//		vueProduct.HasOptions = otherAttributes.HasOptions
//		vueProduct.URLKey = otherAttributes.URLKey
//		vueProduct.TaxClassID = extractInt(otherAttributes.TaxClassID)
//
//		vueProductList = append(vueProductList, vueProduct) }
//	}
//	return vueProductList, err
//}

func VueProductListMapper(productList []SPProduct) ([]byte, error) {
	var vueProduct = NewVueProduct()
	var vueProductList []SimpleProductStruct
	var err error

	var otherAttributes OtherAttributes

	for index := range productList {
		salonProduct := productList[index]
		err = json.Unmarshal(salonProduct.OtherAttributes, &otherAttributes)
		helpers.CheckErr(err)

		if salonProduct.Manufacturer == false {
			salonProduct.Manufacturer = nil
		}

		if salonProduct.Brand == false {
			salonProduct.Brand = nil
		}

		if salonProduct.IsInStock == 0 {
			salonProduct.IsInStock = false
		}
		if salonProduct.IsInStock == 1 {
			salonProduct.IsInStock = true
		}

		if salonProduct.Supplier == false {
			salonProduct.Supplier = nil
		}

		if salonProduct.TypeID == "virtual" {
			fmt.Println("product sku: " + salonProduct.Sku + " was virtual")
		}

		if salonProduct.TypeID == "configurable" {
			fmt.Println("product sku: " + salonProduct.Sku + " was virtual")
		} else {
			vueProduct.ID = salonProduct.EntityID
			//testing change
			vueProduct.DocType = "parentDoc"
			vueProduct.Source.Sku = salonProduct.Sku
			vueProduct.Source.Name = salonProduct.Name
			vueProduct.Source.AttributeSetID = extractInt(otherAttributes.AttributeSetID)
			vueProduct.Source.Price = extractPrice(salonProduct.Price)
			//vueProduct.Source.Status = extractInt(salonProduct.Status)
			vueProduct.Source.Status = 1
			vueProduct.Source.Visibility = extractInt(salonProduct.Visibility)
			vueProduct.Source.TypeID = salonProduct.TypeID
			vueProduct.Source.CreatedAt = salonProduct.CreatedAt
			vueProduct.Source.UpdatedAt = salonProduct.UpdatedAt
			vueProduct.Source.FinalPrice = extractPrice(salonProduct.Price)
			vueProduct.Source.MaxPrice = extractPrice(salonProduct.Price)
			vueProduct.Source.MaxRegularPrice = extractPrice(salonProduct.Price)
			vueProduct.Source.MinimalRegularPrice = extractPrice(salonProduct.Price)
			vueProduct.Source.MinimalPrice = extractPrice(salonProduct.Price)
			vueProduct.Source.RegularPrice = extractPrice(salonProduct.Price)
			vueProduct.Source.Stock.ItemID = extractInt(salonProduct.EntityID)
			vueProduct.Source.Stock.ProductID = extractInt(salonProduct.EntityID)
			//testing change
			vueProduct.Source.Stock.StockID = 1
			vueProduct.Source.Stock.Qty = salonProduct.Qty
			vueProduct.Source.Stock.IsInStock = extractBool(salonProduct.IsInStock)
			vueProduct.Source.Description = salonProduct.Description
			vueProduct.Source.Image = otherAttributes.Image
			vueProduct.Source.SmallImage = otherAttributes.SmallImage
			vueProduct.Source.Thumbnail = otherAttributes.Thumbnail
			vueProduct.Source.OptionsContainer = otherAttributes.OptionsContainer
			vueProduct.Source.RequiredOptions = otherAttributes.RequiredOptions
			vueProduct.Source.HasOptions = otherAttributes.HasOptions
			vueProduct.Source.URLKey = otherAttributes.URLKey
			vueProduct.Source.TaxClassID = otherAttributes.TaxClassID

			// media_gallery

			var mediaGallery MediaGallery
			mediaGallery.TypeOf = "media_gallery"
			mediaGallery.Image = otherAttributes.Image
			mediaGallery.Pos = 1
			mediaGallery.Typ = "image"
			mediaGallery.Lab = ""
			vueProduct.Source.ChildDocuments = append(vueProduct.Source.ChildDocuments, &mediaGallery)

			//for i := range salonProduct.MediaGallery {
			//
			//	salonProduct.MediaGallery[i].TypeOf = "media_gallery"
			//
			//	vueProduct.Source.ChildDocuments = append(vueProduct.Source.ChildDocuments, salonProduct.MediaGallery[i])
			//}

			// category
			//for i := range salonProduct.Category {
			//
			//	product.Source.Category[i].TypeOf = "category"
			//
			//	vueProduct.Source.ChildDocuments = append(vueProduct.Source.ChildDocuments, product.Source.Category[i])
			//}

			// productLinks
			//for i := range product.Source.ProductLinks {
			//
			//	product.Source.ProductLinks[i].TypeOf = "product_links"
			//
			//	vueProduct.Source.ChildDocuments = append(vueProduct.Source.ChildDocuments, product.Source.ProductLinks[i])
			//}

			//Tier_Prices
			//for i := range product.Source.TierPrices {
			//	product.Source.TierPrices[i].TypeOf = "tier_prices"
			//
			//	vueProduct.Source.ChildDocuments = append(vueProduct.Source.ChildDocuments, product.Source.TierPrices[i])
			//}

			vueProductList = append(vueProductList, vueProduct)
		}
	}
	dataJson, err := json.Marshal(vueProductList)
	jsonFile, err := os.Create("./data.json")
	jsonFile.Write(dataJson)
	helpers.CheckErr(err)
	return dataJson, err
}

//func VueProductMapper(salonProduct SPProduct) VueProduct{
//	var vueProduct = DefaultVueProduct
//
//	var otherAttributes OtherAttributes
//	err := json.Unmarshal(salonProduct.OtherAttributes, &otherAttributes)
//	helpers.CheckErr(err)
//
//	vueProduct.ID = extractInt(salonProduct.EntityID)
//	vueProduct.Sku = salonProduct.Sku
//	vueProduct.Name = salonProduct.Name
//	vueProduct.AttributeSetID = extractInt(otherAttributes.AttributeSetID)
//	vueProduct.Price = extractPrice(salonProduct.Price)
//	vueProduct.Status = extractInt(salonProduct.Status)
//	vueProduct.Visibility = extractInt(salonProduct.Visibility)
//	vueProduct.TypeID = salonProduct.TypeID
//	vueProduct.CreatedAt = salonProduct.CreatedAt
//	vueProduct.UpdatedAt = salonProduct.UpdatedAt
//	vueProduct.FinalPrice = extractPrice(salonProduct.Price)
//	vueProduct.MaxPrice = extractPrice(salonProduct.Price)
//	vueProduct.MaxRegularPrice = extractPrice(salonProduct.Price)
//	vueProduct.MinimalRegularPrice = extractPrice(salonProduct.Price)
//	vueProduct.MinimalPrice = extractPrice(salonProduct.Price)
//	vueProduct.RegularPrice = extractPrice(salonProduct.Price)
//	vueProduct.ItemID = extractInt(salonProduct.EntityID)
//	vueProduct.ProductID = extractInt(salonProduct.EntityID)
//	vueProduct.Qty = salonProduct.Qty
//	vueProduct.IsInStock = extractBool(salonProduct.IsInStock)
//	vueProduct.Description = salonProduct.Description
//	vueProduct.Image = otherAttributes.Image
//	vueProduct.SmallImage = otherAttributes.SmallImage
//	vueProduct.Thumbnail = otherAttributes.Thumbnail
//	vueProduct.OptionsContainer = otherAttributes.OptionsContainer
//	vueProduct.RequiredOptions = otherAttributes.RequiredOptions
//	vueProduct.HasOptions = otherAttributes.HasOptions
//	vueProduct.URLKey = otherAttributes.URLKey
//	vueProduct.TaxClassID = extractInt(otherAttributes.TaxClassID)
//
//	return vueProduct
//}
