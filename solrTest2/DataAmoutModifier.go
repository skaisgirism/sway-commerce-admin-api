package solrTest2

import (
	"bitbucket.org/sway-commerce/sway-commerce-admin-api/solrTest2/vueMapper"
	"fmt"
	"strconv"
)

// Function that takes SolrDataStruct parameter,
// increases amount of data stored in []SimpleProductStruct
// and []ConfigurableProductStruct to int got from amount parameter
func modifyDataAmount(dataStruct vueMapper.SolrDataStruct, amount int) vueMapper.SolrDataStruct {
	var simpleProductArray []vueMapper.SimpleProductStruct
	//var configurableProductArray []vueMapper.ConfigurableProductStruct
	var initialSimpleProductArrayLength = len(dataStruct.SimpleProductStruct) - 1
	//var initialConfigurableArrayLength = len(dataStruct.ConfigurableProductStruct) - 1

	//More simple products
	for i := amount; i > 0; i-- {
		if initialSimpleProductArrayLength >= 0 {
			dataStruct.SimpleProductStruct[initialSimpleProductArrayLength].ID = "simple_product_" + strconv.Itoa(i)
			dataStruct.SimpleProductStruct[initialSimpleProductArrayLength].Source.ID = i
			simpleProductArray = append(simpleProductArray, dataStruct.SimpleProductStruct[initialSimpleProductArrayLength])
			if initialSimpleProductArrayLength > 0 {
				initialSimpleProductArrayLength = initialSimpleProductArrayLength - 1
			} else {
				initialSimpleProductArrayLength = len(dataStruct.SimpleProductStruct) - 1
			}
		}
	}

	//More configurable products
	//for i := amount; i > 0; i-- {
	//	if initialConfigurableArrayLength >= 0 {
	//		//----
	//		myCopy := dataStruct.ConfigurableProductStruct[initialConfigurableArrayLength]
	//		myCopy.Source.ChildDocuments = make([]interface{}, len(dataStruct.ConfigurableProductStruct[initialConfigurableArrayLength].Source.ChildDocuments))
	//		copy(myCopy.Source.ChildDocuments, dataStruct.ConfigurableProductStruct[initialConfigurableArrayLength].Source.ChildDocuments)
	//		myCopy.ID = "configurable_product_" + strconv.Itoa(amount+i+1)
	//		myCopy.Source.ID = amount + i + 1
	//		configurableProductArray = append(configurableProductArray, myCopy)
	//
	//		//---
	//		//dataStruct.ConfigurableProductStruct[initialConfigurableArrayLength].ID = "configurable_product_" + strconv.Itoa(amount+i+1)
	//		//dataStruct.ConfigurableProductStruct[initialConfigurableArrayLength].Source.ID = amount + i + 1
	//		//configurableProductArray = append(configurableProductArray, dataStruct.ConfigurableProductStruct[initialConfigurableArrayLength])
	//		if initialConfigurableArrayLength > 0 {
	//			initialConfigurableArrayLength = initialConfigurableArrayLength - 1
	//		} else {
	//			initialConfigurableArrayLength = len(dataStruct.ConfigurableProductStruct) - 1
	//		}
	//	}
	//}
	//// fmt.Println(configurableProductArray[5].Source.ChildDocuments[4].(ConfigurableChildren).ID)
	//
	//z := amount * 3 + 1
	//for i := range configurableProductArray {
	//	for j := range configurableProductArray[i].Source.ChildDocuments {
	//		v, ok := configurableProductArray[i].Source.ChildDocuments[j].(vueMapper.ConfigurableChildren)
	//		if ok {
	//			v.ID = z
	//			z = z + 1
	//			configurableProductArray[i].Source.ChildDocuments[j] = v
	//			//fmt.Println(d.ID, " - ParentID ", configurableProductArray[i].Source.ID)
	//		}
	//
	//		h, ok := configurableProductArray[i].Source.ChildDocuments[j].(ConfigurableOptions)
	//		if ok {
	//			h.ProductID = configurableProductArray[i].Source.ID
	//			configurableProductArray[i].Source.ChildDocuments[j] = h
	//		}
	//	}
	//}

	//Modified data assigned to rest of data
	dataStruct.SimpleProductStruct = simpleProductArray
	//dataStruct.ConfigurableProductStruct = configurableProductArray

	fmt.Println("Simple product length: ", len(dataStruct.SimpleProductStruct))
	fmt.Println("Configurable product length: ", len(dataStruct.ConfigurableProductStruct))

	return dataStruct

}
