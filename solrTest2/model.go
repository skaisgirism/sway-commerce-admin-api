package solrTest2

//
//import (
//	"encoding/json"
//	"github.com/shopspring/decimal"
//)
//
//type SPProduct struct {
//	EntityID             string          `json:"entity_id"`
//	Sku                  string          `json:"sku"`
//	Name                 string          `json:"name"`
//	URL                  string          `json:"url"`
//	Price                string          `json:"price"`
//	Qty                  int             `json:"qty"`
//	IsInStock            interface{}          `json:"is_in_stock"`
//	Status               string          `json:"status"`
//	CreatedAt            string          `json:"created_at"`
//	UpdatedAt            string          `json:"updated_at"`
//	Description          string          `json:"description"`
//	ShortDescription     string          `json:"short_description"`
//	Brand                interface{}          `json:"brand"`
//	Supplier             interface{}          `json:"supplier"`
//	Color                string          `json:"color"`
//	Size                 string          `json:"size"`
//	Manufacturer         interface{}          `json:"manufacturer"`
//	CountryOfManufacture string          `json:"country_of_manufacture"`
//	SpecialPrice         string          `json:"special_price"`
//	GroupPrice           string          `json:"group_price"`
//	MediaGallery         string          `json:"media_gallery"`
//	Weight               string          `json:"weight"`
//	MetaTitle            string          `json:"meta_title"`
//	MetaKeywords         string          `json:"meta_keywords"`
//	MetaDescription      string          `json:"meta_description"`
//	TaxClass             string          `json:"tax_class"`
//	TypeID               string          `json:"type_id"`
//	Visibility           string          `json:"visibility"`
//	OtherAttributes      json.RawMessage `json:"other_attributes"`
//}
//
//type OtherAttributes struct {
//	AttributeSetID      string `json:"attribute_set_id"`
//	HasOptions          string `json:"has_options"`
//	RequiredOptions     string `json:"required_options"`
//	Image               string `json:"image"`
//	SmallImage          string `json:"small_image"`
//	Thumbnail           string `json:"thumbnail"`
//	URLKey              string `json:"url_key"`
//	URLPath             string `json:"url_path"`
//	OptionsContainer    string `json:"options_container"`
//	FoundInRivileAPI    string `json:"found_in_rivile_api"`
//	TaxClassID          string `json:"tax_class_id"`
//	Date                string `json:"date"`
//	SpecialFromDate     string `json:"special_from_date"`
//	SpecialToDate       string `json:"special_to_date"`
//	NewsFromDate        string `json:"news_from_date"`
//	NewsToDate          string `json:"news_to_date"`
//	IsSalable           string `json:"is_salable"`
//	ImageLabel          string `json:"image_label"`
//	SmallImageLabel     string `json:"small_image_label"`
//	ThumbnailLabel      string `json:"thumbnail_label"`
//	Provider            string `json:"provider,omitempty"`
//	SoldToProfessionals string `json:"sold_to_professionals,omitempty"`
//	BlakstienuPlotis    string `json:"blakstienu_plotis"`
//	BlakstienuIlgis     string `json:"blakstienu_ilgis"`
//}
//
//type VueProduct struct {
//	Index                          string          `json:"_index"`
//	Type                           string          `json:"_type"`
//	Score                          int             `json:"_score"`
//	DocType                        string          `json:"doc_type"`
//	ID                             int             `json:"id"`
//	Sku                            string          `json:"sku"`
//	Name                           string          `json:"name"`
//	AttributeSetID                 int             `json:"attribute_set_id"`
//	Price                          decimal.Decimal `json:"price"`
//	Status                         int             `json:"status"`
//	Visibility                     int             `json:"visibility"`
//	TypeID                         string          `json:"type_id"`
//	CreatedAt                      string          `json:"created_at"`
//	UpdatedAt                      string          `json:"updated_at"`
//	CustomAttributes               interface{}     `json:"custom_attributes"`
//	FinalPrice                     decimal.Decimal `json:"final_price"`
//	MaxPrice                       decimal.Decimal `json:"max_price"`
//	MaxRegularPrice                decimal.Decimal `json:"max_regular_price"`
//	MinimalRegularPrice            decimal.Decimal `json:"minimal_regular_price"`
//	MinimalPrice                   decimal.Decimal `json:"minimal_price"`
//	RegularPrice                   decimal.Decimal `json:"regular_price"`
//	ItemID                         int             `json:"item_id"`
//	ProductID                      int             `json:"product_id"`
//	StockID                        int             `json:"stock_id"`
//	Qty                            int             `json:"qty"`
//	IsInStock                      bool            `json:"is_in_stock"`
//	IsQtyDecimal                   bool            `json:"is_qty_decimal"`
//	ShowDefaultNotificationMessage bool            `json:"show_default_notification_message"`
//	UseConfigMinQty                bool            `json:"use_config_min_qty"`
//	MinQty                         int             `json:"min_qty"`
//	UseConfigMinSaleQty            int             `json:"use_config_min_sale_qty"`
//	MinSaleQty                     int             `json:"min_sale_qty"`
//	UseConfigMaxSaleQty            bool            `json:"use_config_max_sale_qty"`
//	MaxSaleQty                     int             `json:"max_sale_qty"`
//	UseConfigBackorders            bool            `json:"use_config_backorders"`
//	Backorders                     int             `json:"backorders"`
//	UseConfigNotifyStockQty        bool            `json:"use_config_notify_stock_qty"`
//	NotifyStockQty                 int             `json:"notify_stock_qty"`
//	UseConfigQtyIncrements         bool            `json:"use_config_qty_increments"`
//	QtyIncrements                  int             `json:"qty_increments"`
//	UseConfigEnableQtyInc          bool            `json:"use_config_enable_qty_inc"`
//	EnableQtyIncrements            bool            `json:"enable_qty_increments"`
//	UseConfigManageStock           bool            `json:"use_config_manage_stock"`
//	ManageStock                    bool            `json:"manage_stock"`
//	LowStockDate                   interface{}     `json:"low_stock_date"`
//	IsDecimalDivided               bool            `json:"is_decimal_divided"`
//	StockStatusChangedAuto         int             `json:"stock_status_changed_auto"`
//	Tsk                            int64           `json:"tsk"`
//	Description                    string          `json:"description"`
//	Image                          string          `json:"image"`
//	SmallImage                     string          `json:"small_image"`
//	Thumbnail                      string          `json:"thumbnail"`
//	CategoryIds                    []int           `json:"category_ids"`
//	OptionsContainer               string          `json:"options_container"`
//	RequiredOptions                string          `json:"required_options"`
//	HasOptions                     string          `json:"has_options"`
//	URLKey                         string          `json:"url_key"`
//	TaxClassID                     int             `json:"tax_class_id"`
//	Activity                       string          `json:"activity,omitempty"`
//	Material                       string          `json:"material,omitempty"`
//	Gender                         string          `json:"gender"`
//	CategoryGear                   string          `json:"category_gear"`
//	ErinRecommends                 string          `json:"erin_recommends"`
//	New                            string          `json:"new"`
//	Sale                           string          `json:"sale"`
//	ChildDocuments                 []interface{}   `json:"_childDocuments_,omitempty"`
//}
//
//type SolrDataStruct struct {
//	SimpleProductStruct       []SimpleProductStruct
//	ConfigurableProductStruct []ConfigurableProductStruct
//	CategoryStruct            []CategoryStruct
//	AttributeStruct           []AttributeStruct
//	TaxRuleStruct             []TaxRuleStruct
//}
//
//type SimpleProductStruct struct {
//	Index   string `json:"_index"`
//	Type    string `json:"_type"`
//	ID      string `json:"_id"`
//	Score   int    `json:"_score"`
//	DocType string `json:"doc_type"`
//	Source  struct {
//		ID                  int         `json:"id"`
//		Sku                 string      `json:"sku"`
//		Name                string      `json:"name"`
//		AttributeSetID      int         `json:"attribute_set_id"`
//		Price               decimal.Decimal     `json:"price"`
//		Status              int         `json:"status"`
//		Visibility          int         `json:"visibility"`
//		TypeID              string      `json:"type_id"`
//		CreatedAt           string      `json:"created_at"`
//		UpdatedAt           string      `json:"updated_at"`
//		CustomAttributes    interface{} `json:"custom_attributes"`
//		FinalPrice          decimal.Decimal     `json:"final_price"`
//		MaxPrice            decimal.Decimal     `json:"max_price"`
//		MaxRegularPrice     decimal.Decimal     `json:"max_regular_price"`
//		MinimalRegularPrice decimal.Decimal     `json:"minimal_regular_price"`
//		MinimalPrice        decimal.Decimal     `json:"minimal_price"`
//		RegularPrice        decimal.Decimal     `json:"regular_price"`
//		Stock               struct {
//			ItemID                         int         `json:"item_id"`
//			ProductID                      int         `json:"product_id"`
//			StockID                        int         `json:"stock_id"`
//			Qty                            int         `json:"qty"`
//			IsInStock                      bool        `json:"is_in_stock"`
//			IsQtyDecimal                   bool        `json:"is_qty_decimal"`
//			ShowDefaultNotificationMessage bool        `json:"show_default_notification_message"`
//			UseConfigMinQty                bool        `json:"use_config_min_qty"`
//			MinQty                         int         `json:"min_qty"`
//			UseConfigMinSaleQty            int         `json:"use_config_min_sale_qty"`
//			MinSaleQty                     int         `json:"min_sale_qty"`
//			UseConfigMaxSaleQty            bool        `json:"use_config_max_sale_qty"`
//			MaxSaleQty                     int         `json:"max_sale_qty"`
//			UseConfigBackorders            bool        `json:"use_config_backorders"`
//			Backorders                     int         `json:"backorders"`
//			UseConfigNotifyStockQty        bool        `json:"use_config_notify_stock_qty"`
//			NotifyStockQty                 int         `json:"notify_stock_qty"`
//			UseConfigQtyIncrements         bool        `json:"use_config_qty_increments"`
//			QtyIncrements                  int         `json:"qty_increments"`
//			UseConfigEnableQtyInc          bool        `json:"use_config_enable_qty_inc"`
//			EnableQtyIncrements            bool        `json:"enable_qty_increments"`
//			UseConfigManageStock           bool        `json:"use_config_manage_stock"`
//			ManageStock                    bool        `json:"manage_stock"`
//			LowStockDate                   interface{} `json:"low_stock_date"`
//			IsDecimalDivided               bool        `json:"is_decimal_divided"`
//			StockStatusChangedAuto         int         `json:"stock_status_changed_auto"`
//		} `json:"stock"`
//		Tsk              int64         `json:"tsk"`
//		Description      string        `json:"description"`
//		Image            string        `json:"image"`
//		SmallImage       string        `json:"small_image"`
//		Thumbnail        string        `json:"thumbnail"`
//		CategoryIds      []string      `json:"category_ids"`
//		OptionsContainer string        `json:"options_container"`
//		RequiredOptions  string        `json:"required_options"`
//		HasOptions       string        `json:"has_options"`
//		URLKey           string        `json:"url_key"`
//		TaxClassID       string        `json:"tax_class_id"`
//		Activity         string        `json:"activity,omitempty"`
//		Material         string        `json:"material,omitempty"`
//		Gender           string        `json:"gender"`
//		CategoryGear     string        `json:"category_gear"`
//		ErinRecommends   string        `json:"erin_recommends"`
//		New              string        `json:"new"`
//		ChildDocuments   []interface{} `json:"_childDocuments_"`
//	} `json:"_source"`
//}
//
//type ConfigurableProductStruct struct {
//	Index   string `json:"_index"`
//	Type    string `json:"_type"`
//	ID      string `json:"_id"`
//	Score   int    `json:"_score"`
//	DocType string `json:"doc_type"`
//	Source  struct {
//		ID                  int         `json:"id"`
//		Sku                 string      `json:"sku"`
//		Name                string      `json:"name"`
//		AttributeSetID      int         `json:"attribute_set_id"`
//		Price               float64     `json:"price"`
//		Status              int         `json:"status"`
//		Visibility          int         `json:"visibility"`
//		TypeID              string      `json:"type_id"`
//		CreatedAt           string      `json:"created_at"`
//		UpdatedAt           string      `json:"updated_at"`
//		CustomAttributes    interface{} `json:"custom_attributes"`
//		FinalPrice          float64     `json:"final_price"`
//		MaxPrice            float64     `json:"max_price"`
//		MaxRegularPrice     float64     `json:"max_regular_price"`
//		MinimalRegularPrice float64     `json:"minimal_regular_price"`
//		MinimalPrice        float64     `json:"minimal_price"`
//		RegularPrice        float64     `json:"regular_price"`
//		Stock               struct {
//			ItemID                         int         `json:"item_id"`
//			ProductID                      int         `json:"product_id"`
//			StockID                        int         `json:"stock_id"`
//			Qty                            int         `json:"qty"`
//			IsInStock                      bool        `json:"is_in_stock"`
//			IsQtyDecimal                   bool        `json:"is_qty_decimal"`
//			ShowDefaultNotificationMessage bool        `json:"show_default_notification_message"`
//			UseConfigMinQty                bool        `json:"use_config_min_qty"`
//			MinQty                         int         `json:"min_qty"`
//			UseConfigMinSaleQty            int         `json:"use_config_min_sale_qty"`
//			MinSaleQty                     int         `json:"min_sale_qty"`
//			UseConfigMaxSaleQty            bool        `json:"use_config_max_sale_qty"`
//			MaxSaleQty                     int         `json:"max_sale_qty"`
//			UseConfigBackorders            bool        `json:"use_config_backorders"`
//			Backorders                     int         `json:"backorders"`
//			UseConfigNotifyStockQty        bool        `json:"use_config_notify_stock_qty"`
//			NotifyStockQty                 int         `json:"notify_stock_qty"`
//			UseConfigQtyIncrements         bool        `json:"use_config_qty_increments"`
//			QtyIncrements                  int         `json:"qty_increments"`
//			UseConfigEnableQtyInc          bool        `json:"use_config_enable_qty_inc"`
//			EnableQtyIncrements            bool        `json:"enable_qty_increments"`
//			UseConfigManageStock           bool        `json:"use_config_manage_stock"`
//			ManageStock                    bool        `json:"manage_stock"`
//			LowStockDate                   interface{} `json:"low_stock_date"`
//			IsDecimalDivided               bool        `json:"is_decimal_divided"`
//			StockStatusChangedAuto         int         `json:"stock_status_changed_auto"`
//		} `json:"stock"`
//		ColorOptions               []int         `json:"color_options"`
//		SizeOptions                []int         `json:"size_options"`
//		Tsk                        int64         `json:"tsk"`
//		Description                string        `json:"description"`
//		Image                      string        `json:"image"`
//		SmallImage                 string        `json:"small_image"`
//		Thumbnail                  string        `json:"thumbnail"`
//		CategoryIds                []string      `json:"category_ids"`
//		OptionsContainer           string        `json:"options_container"`
//		RequiredOptions            string        `json:"required_options"`
//		HasOptions                 string        `json:"has_options"`
//		URLKey                     string        `json:"url_key"`
//		MsrpDisplayActualPriceType string        `json:"msrp_display_actual_price_type"`
//		TaxClassID                 string        `json:"tax_class_id,omitempty"`
//		Material                   string        `json:"material"`
//		EcoCollection              string        `json:"eco_collection"`
//		PerformanceFabric          string        `json:"performance_fabric"`
//		ErinRecommends             string        `json:"erin_recommends"`
//		New                        string        `json:"new"`
//		Sale                       string        `json:"sale"`
//		Pattern                    string        `json:"pattern"`
//		Climate                    string        `json:"climate"`
//		ChildDocuments             []interface{} `json:"_childDocuments_"`
//	} `json:"_source"`
//}
//
//type CategoryStruct struct {
//	Index  string `json:"_index"`
//	Type   string `json:"_type"`
//	ID     string `json:"_id"`
//	Score  int    `json:"_score"`
//	Source struct {
//		ID           int    `json:"id"`
//		ParentID     int    `json:"parent_id"`
//		Name         string `json:"name"`
//		IsActive     bool   `json:"is_active"`
//		Position     int    `json:"position"`
//		Level        int    `json:"level"`
//		ProductCount int    `json:"product_count"`
//		//ChildrenData    []ChildrenData `json:"children_data"`
//		Children        string        `json:"children"`
//		CreatedAt       string        `json:"created_at"`
//		UpdatedAt       string        `json:"updated_at"`
//		Path            string        `json:"path"`
//		AvailableSortBy []interface{} `json:"available_sort_by"`
//		IncludeInMenu   bool          `json:"include_in_menu"`
//		IsAnchor        string        `json:"is_anchor"`
//		ChildrenCount   string        `json:"children_count"`
//		URLKey          string        `json:"url_key"`
//		URLPath         string        `json:"url_path"`
//		Tsk             int64         `json:"tsk"`
//		ChildDocuments  []interface{} `json:"_childDocuments_"`
//	} `json:"_source"`
//}
//
//type AttributeStruct struct {
//	Index  string `json:"_index"`
//	Type   string `json:"_type"`
//	ID     string `json:"_id"`
//	Score  int    `json:"_score"`
//	Source struct {
//		IsWysiwygEnabled          bool          `json:"is_wysiwyg_enabled"`
//		IsHTMLAllowedOnFront      bool          `json:"is_html_allowed_on_front"`
//		UsedForSortBy             bool          `json:"used_for_sort_by"`
//		IsFilterable              bool          `json:"is_filterable"`
//		IsFilterableInSearch      bool          `json:"is_filterable_in_search"`
//		IsUsedInGrid              bool          `json:"is_used_in_grid"`
//		IsVisibleInGrid           bool          `json:"is_visible_in_grid"`
//		IsFilterableInGrid        bool          `json:"is_filterable_in_grid"`
//		Position                  int           `json:"position"`
//		ApplyTo                   []interface{} `json:"apply_to"`
//		IsSearchable              string        `json:"is_searchable"`
//		IsVisibleInAdvancedSearch string        `json:"is_visible_in_advanced_search"`
//		IsComparable              string        `json:"is_comparable"`
//		IsUsedForPromoRules       string        `json:"is_used_for_promo_rules"`
//		IsVisibleOnFront          string        `json:"is_visible_on_front"`
//		UsedInProductListing      string        `json:"used_in_product_listing"`
//		IsVisible                 bool          `json:"is_visible"`
//		Scope                     string        `json:"scope"`
//		AttributeID               int           `json:"attribute_id"`
//		AttributeCode             string        `json:"attribute_code"`
//		FrontendInput             string        `json:"frontend_input"`
//		EntityTypeID              string        `json:"entity_type_id"`
//		IsRequired                bool          `json:"is_required"`
//		IsUserDefined             bool          `json:"is_user_defined"`
//		DefaultFrontendLabel      string        `json:"default_frontend_label"`
//		FrontendLabels            interface{}   `json:"frontend_labels"`
//		BackendType               string        `json:"backend_type"`
//		BackendModel              string        `json:"backend_model"`
//		SourceModel               string        `json:"source_model"`
//		DefaultValue              string        `json:"default_value"`
//		IsUnique                  string        `json:"is_unique"`
//		ValidationRules           []interface{} `json:"validation_rules"`
//		ID                        int           `json:"id"`
//		Tsk                       int64         `json:"tsk"`
//		ChildDocuments            []interface{} `json:"_childDocuments_"`
//	} `json:"_source"`
//}
//
//type TaxRuleStruct struct {
//	Index  string `json:"_index"`
//	Type   string `json:"_type"`
//	ID     string `json:"_id"`
//	Score  int    `json:"_score"`
//	Source struct {
//		ID                  int           `json:"id"`
//		Code                string        `json:"code"`
//		Priority            int           `json:"priority"`
//		Position            int           `json:"position"`
//		CustomerTaxClassIds []int         `json:"customer_tax_class_ids"`
//		ProductTaxClassIds  []int         `json:"product_tax_class_ids"`
//		TaxRateIds          []int         `json:"tax_rate_ids"`
//		CalculateSubtotal   bool          `json:"calculate_subtotal"`
//		Tsk                 int64         `json:"tsk"`
//		ChildDocuments      []interface{} `json:"_childDocuments_"`
//	} `json:"_source"`
//}
//
//
