package helpers

import (
	sql_ "database/sql"
	"encoding/json"
	"github.com/jmoiron/sqlx"
	log "github.com/sirupsen/logrus"
	"io"
	"net/http"
	"os"
	"strconv"
)

const HeaderContentType = "Content-Type"
const MIMEApplicationJSON = "application/json; charset=utf-8"

func PanicErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func CheckErr(err error) bool {
	if err != nil {
		log.Println("Error: ", err)
		return false
	}
	return true
}

func CheckErrHttp(err error, w http.ResponseWriter) {
	if err != nil {
		http.Error(w, err.Error(), 500)
		log.Println("Error: ", err)
	}
}

func SendJsonError(err error, w http.ResponseWriter) {
	if err != nil {
		http.Error(w, err.Error(), 500)
		log.Println("Error: ", err)
	}
}

func SendJSON(w http.ResponseWriter, httpStatus int, object interface{}) {
	w.Header().Set(HeaderContentType, MIMEApplicationJSON)
	jsonData, err := json.Marshal(object)
	if err != nil {
		log.Errorf("error on result marshal to json. %s", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(httpStatus)
	_, err = w.Write(jsonData)
	if err != nil {
		log.Errorf("error on writing json result to ResponseWriter. %s", err)
	}
}

func SendJsonOk(w http.ResponseWriter, object interface{}) {
	SendJSON(w, http.StatusOK, object)
}

func PanicErrHttp(err error, w http.ResponseWriter) {
	if err != nil {
		http.Error(w, err.Error(), 500)
		w.WriteHeader(http.StatusInternalServerError)
		log.Println("Error: ", err)
	}
}

func CheckIfRowExistsInMysql(dataBaseName *sqlx.DB, tableName string, fieldName string, fieldValue string) string {
	var value string
	res := dataBaseName.QueryRow("SELECT "+fieldName+" FROM "+tableName+" WHERE "+fieldName+" = ?", fieldValue)
	err := res.Scan(&value)
	PanicErr(err)
	return value
}

func InitLogRus() {
	file, err := os.OpenFile("log.txt", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	PanicErr(err)
	mw := io.MultiWriter(os.Stdout, file)
	log.SetOutput(mw)
}

func CloseMySqlRows(rows *sql_.Rows) {
	err := rows.Close()
	PanicErr(err)
}

//gets int from string
func ExtractInt(value string) int {
	result, err := strconv.Atoi(value)
	CheckErr(err)
	return result
}