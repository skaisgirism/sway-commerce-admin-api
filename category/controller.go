package category

import (
	c "bitbucket.org/sway-commerce/sway-commerce-admin-api/config"
	"bitbucket.org/sway-commerce/sway-commerce-admin-api/helpers"
	sql_ "database/sql"
	"encoding/json"
	"fmt"
	"github.com/go-chi/chi"
	"net/http"
	"strings"
)

func createCategory(w http.ResponseWriter, r *http.Request) {
	var category Category
	err := json.NewDecoder(r.Body).Decode(&category)
	helpers.PanicErr(err)

	db, err := c.Conf.GetDb()
	helpers.PanicErr(err)

	result, err := db.Exec("INSERT INTO category("+
		"entity_id, "+
		"parent_id, "+
		"created_at, "+
		"updated_at, "+
		"path, "+
		"position, "+
		"level, "+
		"children_count, "+
		"store_id, "+
		"addtocart, "+
		"all_children, "+
		"available_sort_by, "+
		"category_description_tpl, "+
		"category_meta_description_tpl, "+
		"category_meta_keywords_tpl, "+
		"category_meta_title_tpl, "+
		"category_title_tpl, "+
		"children, "+
		"compareproducts, "+
		"custom_apply_to_products, "+
		"custom_design, "+
		"custom_design_from, "+
		"custom_design_to, "+
		"custom_layout_update, "+
		"custom_use_parent_settings, "+
		"default_sort_by, "+
		"description, "+
		"display_mode, "+
		"filter_description_tpl, "+
		"filter_meta_description_tpl, "+
		"filter_meta_keywords_tpl, "+
		"filter_meta_title_tpl, "+
		"filter_price_range, "+
		"filter_title_tpl, "+
		"image, "+
		"include_in_menu, "+
		"is_active, "+
		"is_anchor, "+
		"landing_page, "+
		"mailtofriend, "+
		"meta_description, "+
		"meta_keywords, "+
		"meta_title, "+
		"name, "+
		"page_layout, "+
		"path_in_store, "+
		"productname, "+
		"productprice, "+
		"product_description_tpl, "+
		"product_meta_description_tpl, "+
		"product_meta_keywords_tpl, "+
		"product_meta_title_tpl, "+
		"product_title_tpl, "+
		"quickview, "+
		"sidebarhider, "+
		"thumbnail, "+
		"url_key, "+
		"url_path, "+
		"wishlist, "+
		"youtubecode) "+
		" VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
		category.EntityID,
		category.ParentID,
		category.CreatedAt,
		category.UpdatedAt,
		category.Path,
		category.Position,
		category.Level,
		category.ChildrenCount,
		category.StoreID,
		category.Addtocart,
		category.AllChildren,
		category.AvailableSortBy,
		category.CategoryDescriptionTpl,
		category.CategoryMetaDescriptionTpl,
		category.CategoryMetaKeywordsTpl,
		category.CategoryMetaTitleTpl,
		category.CategoryTitleTpl,
		category.Children,
		category.Compareproducts,
		category.CustomApplyToProducts,
		category.CustomDesign,
		category.CustomDesignFrom,
		category.CustomDesignTo,
		category.CustomLayoutUpdate,
		category.CustomUseParentSettings,
		category.DefaultSortBy,
		category.Description,
		category.DisplayMode,
		category.FilterDescriptionTpl,
		category.FilterMetaDescriptionTpl,
		category.FilterMetaKeywordsTpl,
		category.FilterMetaTitleTpl,
		category.FilterPriceRange,
		category.FilterTitleTpl,
		category.Image,
		category.IncludeInMenu,
		category.IsActive,
		category.IsAnchor,
		category.LandingPage,
		category.Mailtofriend,
		category.MetaDescription,
		category.MetaKeywords,
		category.MetaTitle,
		category.Name,
		category.PageLayout,
		category.PathInStore,
		category.Productname,
		category.Productprice,
		category.ProductDescriptionTpl,
		category.ProductMetaDescriptionTpl,
		category.ProductMetaKeywordsTpl,
		category.ProductMetaTitleTpl,
		category.ProductTitleTpl,
		category.Quickview,
		category.Sidebarhider,
		category.Thumbnail,
		category.URLKey,
		category.URLPath,
		category.Wishlist,
		category.Youtubecode)
	fmt.Println(result)
	helpers.PanicErr(err)
}

var fields = []string{"entity_id", "parent_id", "created_at", "updated_at", "path", "position", "level", "children_count", "store_id", "addtocart, " +
	"all_children", "available_sort_by", "category_description_tpl", "category_meta_description_tpl", "category_meta_keywords_tpl", "category_meta_title_tpl, " +
	"category_title_tpl", "children", "compareproducts", "custom_apply_to_products", "custom_design", "custom_design_from, " +
	"custom_design_to", "custom_layout_update", "custom_use_parent_settings", "default_sort_by", "description", "display_mode, " +
	"filter_description_tpl", "filter_meta_description_tpl", "filter_meta_keywords_tpl", "filter_meta_title_tpl", "filter_price_range, " +
	"filter_title_tpl", "image", "include_in_menu", "is_active", "is_anchor", "landing_page", "mailtofriend", "meta_description, " +
	"meta_keywords", "meta_title", "name", "page_layout", "path_in_store", "productname", "productprice", "product_description_tpl, " +
	"product_meta_description_tpl", "product_meta_keywords_tpl", "product_meta_title_tpl", "product_title_tpl", "quickview, " +
	"sidebarhider", "thumbnail", "url_key", "url_path", "wishlist", "youtubecode"}
var fieldsString = strings.Join(fields, ", ")

func getCategory(w http.ResponseWriter, r *http.Request) {
	entityID := chi.URLParam(r, "EntityID")

	db, err := c.Conf.GetDb()
	if err != nil {
		helpers.SendJsonError(err, w)
		return
	}

	category := Category{}
	err = db.Select(&category, "SELECT "+fieldsString+" FROM solrCategory c WHERE entity_id=?", entityID)
	if err == sql_.ErrNoRows {
		helpers.SendJSON(w, http.StatusNotFound, nil)
	} else {
		helpers.SendJsonOk(w, category)
	}
}

func getCategoryList(w http.ResponseWriter, r *http.Request) {
	var category Category
	var categories []Category
	categories = []Category{}

	db, err := c.Conf.GetDb()

	query, err := db.Query("SELECT " +
		"entity_id, " +
		"parent_id, " +
		"created_at, " +
		"updated_at, " +
		"path, " +
		"position, " +
		"level, " +
		"children_count, " +
		"store_id, " +
		"addtocart, " +
		"all_children, " +
		"available_sort_by, " +
		"category_description_tpl, " +
		"category_meta_description_tpl, " +
		"category_meta_keywords_tpl, " +
		"category_meta_title_tpl, " +
		"category_title_tpl, " +
		"children, " +
		"compareproducts, " +
		"custom_apply_to_products, " +
		"custom_design, " +
		"custom_design_from, " +
		"custom_design_to, " +
		"custom_layout_update, " +
		"custom_use_parent_settings, " +
		"default_sort_by, " +
		"description, " +
		"display_mode, " +
		"filter_description_tpl, " +
		"filter_meta_description_tpl, " +
		"filter_meta_keywords_tpl, " +
		"filter_meta_title_tpl, " +
		"filter_price_range, " +
		"filter_title_tpl, " +
		"image, " +
		"include_in_menu, " +
		"is_active, " +
		"is_anchor, " +
		"landing_page, " +
		"mailtofriend, " +
		"meta_description, " +
		"meta_keywords, " +
		"meta_title, " +
		"name, " +
		"page_layout, " +
		"path_in_store, " +
		"productname, " +
		"productprice, " +
		"product_description_tpl, " +
		"product_meta_description_tpl, " +
		"product_meta_keywords_tpl, " +
		"product_meta_title_tpl, " +
		"product_title_tpl, " +
		"quickview, " +
		"sidebarhider, " +
		"thumbnail, " +
		"url_key, " +
		"url_path, " +
		"wishlist, " +
		"youtubecode FROM category")
	helpers.CheckErr(err)
	defer helpers.CloseMySqlRows(query)
	for query.Next() {
		err := query.Scan(&category.EntityID,
			&category.ParentID,
			&category.CreatedAt,
			&category.UpdatedAt,
			&category.Path,
			&category.Position,
			&category.Level,
			&category.ChildrenCount,
			&category.StoreID,
			&category.Addtocart,
			&category.AllChildren,
			&category.AvailableSortBy,
			&category.CategoryDescriptionTpl,
			&category.CategoryMetaDescriptionTpl,
			&category.CategoryMetaKeywordsTpl,
			&category.CategoryMetaTitleTpl,
			&category.CategoryTitleTpl,
			&category.Children,
			&category.Compareproducts,
			&category.CustomApplyToProducts,
			&category.CustomDesign,
			&category.CustomDesignFrom,
			&category.CustomDesignTo,
			&category.CustomLayoutUpdate,
			&category.CustomUseParentSettings,
			&category.DefaultSortBy,
			&category.Description,
			&category.DisplayMode,
			&category.FilterDescriptionTpl,
			&category.FilterMetaDescriptionTpl,
			&category.FilterMetaKeywordsTpl,
			&category.FilterMetaTitleTpl,
			&category.FilterPriceRange,
			&category.FilterTitleTpl,
			&category.Image,
			&category.IncludeInMenu,
			&category.IsActive,
			&category.IsAnchor,
			&category.LandingPage,
			&category.Mailtofriend,
			&category.MetaDescription,
			&category.MetaKeywords,
			&category.MetaTitle,
			&category.Name,
			&category.PageLayout,
			&category.PathInStore,
			&category.Productname,
			&category.Productprice,
			&category.ProductDescriptionTpl,
			&category.ProductMetaDescriptionTpl,
			&category.ProductMetaKeywordsTpl,
			&category.ProductMetaTitleTpl,
			&category.ProductTitleTpl,
			&category.Quickview,
			&category.Sidebarhider,
			&category.Thumbnail,
			&category.URLKey,
			&category.URLPath,
			&category.Wishlist,
			&category.Youtubecode)
		helpers.CheckErrHttp(err, w)
		categories = append(categories, category)
	}
	err = query.Err()
	helpers.SendJsonOk(w, categories)
}

func deleteCategory(w http.ResponseWriter, r *http.Request) {
	entityID := chi.URLParam(r, "EntityID")
	db, err := c.Conf.GetDb()
	helpers.CheckErr(err)

	res, err := db.Exec("DELETE c FROM category c WHERE c.entity_id=?", entityID)
	fmt.Println(res)
	helpers.CheckErr(err)
}

func updateCategory(w http.ResponseWriter, r *http.Request) {
	entityID := chi.URLParam(r, "EntityID")
	var category Category
	err := json.NewDecoder(r.Body).Decode(&category)
	helpers.PanicErr(err)

	db, err := c.Conf.GetDb()
	helpers.PanicErr(err)

	helpers.CheckIfRowExistsInMysql(db, "category", "entity_id", entityID)

	query, err := db.Prepare("Update category set " +
		"entity_id=?, " +
		"parent_id=?, " +
		"created_at=?, " +
		"updated_at=?, " +
		"path=?, " +
		"position=?, " +
		"level=?, " +
		"children_count=?, " +
		"store_id=?, " +
		"addtocart=?, " +
		"all_children=?, " +
		"available_sort_by=?, " +
		"category_description_tpl=?, " +
		"category_meta_description_tpl=?, " +
		"category_meta_keywords_tpl=?, " +
		"category_meta_title_tpl=?, " +
		"category_title_tpl=?, " +
		"children=?, " +
		"compareproducts=?, " +
		"custom_apply_to_products=?, " +
		"custom_design=?, " +
		"custom_design_from=?, " +
		"custom_design_to=?, " +
		"custom_layout_update=?, " +
		"custom_use_parent_settings=?, " +
		"default_sort_by=?, " +
		"description=?, " +
		"display_mode=?, " +
		"filter_description_tpl=?, " +
		"filter_meta_description_tpl=?, " +
		"filter_meta_keywords_tpl=?, " +
		"filter_meta_title_tpl=?, " +
		"filter_price_range=?, " +
		"filter_title_tpl=?, " +
		"image=?, " +
		"include_in_menu=?, " +
		"is_active=?, " +
		"is_anchor=?, " +
		"landing_page=?, " +
		"mailtofriend=?, " +
		"meta_description=?, " +
		"meta_keywords=?, " +
		"meta_title=?, " +
		"name=?, " +
		"page_layout=?, " +
		"path_in_store=?, " +
		"productname=?, " +
		"productprice=?, " +
		"product_description_tpl=?, " +
		"product_meta_description_tpl=?, " +
		"product_meta_keywords_tpl=?, " +
		"product_meta_title_tpl=?, " +
		"product_title_tpl=?, " +
		"quickview=?, " +
		"sidebarhider=?, " +
		"thumbnail=?, " +
		"url_key=?, " +
		"url_path=?, " +
		"wishlist=?, " +
		"youtubecode=? where entity_id=?")
	helpers.PanicErr(err)

	_, err = query.Exec(category.EntityID,
		category.ParentID,
		category.CreatedAt,
		category.UpdatedAt,
		category.Path,
		category.Position,
		category.Level,
		category.ChildrenCount,
		category.StoreID,
		category.Addtocart,
		category.AllChildren,
		category.AvailableSortBy,
		category.CategoryDescriptionTpl,
		category.CategoryMetaDescriptionTpl,
		category.CategoryMetaKeywordsTpl,
		category.CategoryMetaTitleTpl,
		category.CategoryTitleTpl,
		category.Children,
		category.Compareproducts,
		category.CustomApplyToProducts,
		category.CustomDesign,
		category.CustomDesignFrom,
		category.CustomDesignTo,
		category.CustomLayoutUpdate,
		category.CustomUseParentSettings,
		category.DefaultSortBy,
		category.Description,
		category.DisplayMode,
		category.FilterDescriptionTpl,
		category.FilterMetaDescriptionTpl,
		category.FilterMetaKeywordsTpl,
		category.FilterMetaTitleTpl,
		category.FilterPriceRange,
		category.FilterTitleTpl,
		category.Image,
		category.IncludeInMenu,
		category.IsActive,
		category.IsAnchor,
		category.LandingPage,
		category.Mailtofriend,
		category.MetaDescription,
		category.MetaKeywords,
		category.MetaTitle,
		category.Name,
		category.PageLayout,
		category.PathInStore,
		category.Productname,
		category.Productprice,
		category.ProductDescriptionTpl,
		category.ProductMetaDescriptionTpl,
		category.ProductMetaKeywordsTpl,
		category.ProductMetaTitleTpl,
		category.ProductTitleTpl,
		category.Quickview,
		category.Sidebarhider,
		category.Thumbnail,
		category.URLKey,
		category.URLPath,
		category.Wishlist,
		category.Youtubecode,
		entityID)
	helpers.PanicErr(err)
	if err == nil {
		fmt.Println("Category entity_id: " + category.EntityID + " updated in mysql")
	}
}
