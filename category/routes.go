package category

import (
	"github.com/go-chi/chi"
	"net/http"
)

func RouterCategory() http.Handler {
	r := chi.NewRouter()
	r.Post("/", createCategory)
	r.Get("/{EntityID}", getCategory)
	r.Get("/", getCategoryList)
	r.Delete("/{EntityID}", deleteCategory)
	r.Put("/{EntityID}", updateCategory)
	return r
}
